const moment = require('moment');
const constants = require('./constants');
const electron = require('electron');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
const ipcMain = electron.ipcMain;
const path = require('path');
const isDev = require('electron-is-dev');
const process = require('child_process');
const spawn = require('cross-spawn');
const fs = require('fs');
const csv = require('csv-parser');
const fsx = require('fs-extra');
var glob = require('glob');
const fixPath = require('fix-path');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;
const zipFolder = require('zip-a-folder');
const os = require('os');
let mainWindow;

var scriptPid = {
  buyma: '',
  add_frame: '',
  asos: '',
  farfetch: '',
  louisvuitton: '',
  matchesfashion: '',
  item_checker: '',
};
function createWindow() {
  mainWindow = new BrowserWindow({
    width: 900,
    height: 680,
    webPreferences: {
      nodeIntegration: true,
    },
  });
  mainWindow.loadURL(
    isDev ? 'http://localhost:3000' : `file://${path.join(__dirname, '../build/index.html')}`,
  );

  if (isDev) {
    // Open the DevTools.
    //BrowserWindow.addDevToolsExtension('<location to your react chrome extension>');
    // mainWindow.webContents.openDevTools();
  }
  // mainWindow.on('closed', () => (mainWindow = null));
  mainWindow.webContents.on('will-navigate', (e, redirectUrl) => {
    e.preventDefault();
  });

  ipcMain.on('GET_MAC_ADDRESS', (event, args) => {
    require('node-macaddress').one(function (err, addr) {
      event.reply('GET_MAC_ADDRESS_REPLY', addr);
    });
  });

  ipcMain.on('run-script', (event, args) => {
    let data = [];
    event.preventDefault();
    fixPath();
    event.returnValue = false;
    let cmd = path.join(
      __dirname,
      `scripts/${constants.SCRIPT_NAME.SEARCH_SCRIPT.name}/${args.fileRuby}.rb`,
    );
    if (cmd === null) {
      return;
    }
    data.push(cmd);
    const replyData = {
      code: 0,
      data: null,
    };
    const ls =
      os.platform() === 'darwin'
        ? spawn('ruby', data, { detached: true })
        : spawn('ruby', data, { windowsHide: true });
    scriptPid[args.scriptName] = ls.pid;
    ls.on('close', code => {
      const errorLogPath = path.join(
        __dirname,
        `scripts/${args.scriptName}/${constants.FILE_ERROR_LOG}`,
      );
      if (fs.existsSync(errorLogPath)) {
        replyData.data = fs
          .readFileSync(errorLogPath)
          .toString()
          .split('\n');
        replyData.code = 1;
      } else {
        replyData.data = [];
        replyData.code = 0;
      }
      event.reply(args.reply, replyData);
    });
    ls.stdout.on('data', data => {
      console.log(`data: ${data}`);
    });
    ls.stderr.on('data', data => {
      console.error(`stderr: ${data}`);
    });
  });

  ipcMain.on('write-file-ruby', (event, args) => {
    const filePath = path.join(__dirname, `scripts/${args.scriptName}/${args.fileName}`);
    fs.writeFile(filePath, args.content, function (err) {
      if (!err) {
        let data = [];
        data.push(path.join(__dirname, `scripts/${constants.FILE_COMPILER_RUBY}`));
        data.push(filePath);
        const ls = spawn('ruby', data, { detached: true });
        ls.stderr.on('data', data => {
          console.error(`stderr: ${data}`);
        });
        ls.on('close', () => {
          if (fs.existsSync(filePath)) {
            fs.unlink(filePath, err => {
              if (err) throw err;
            });
          }
        });
      }
    });
  });

  ipcMain.on('write-file-input', (event, args) => {
    const filePath = path.join(__dirname, `scripts/${args.scriptName}/setting/${args.fileName}`);
    fs.writeFile(filePath, args.content, function (err) {
      if (err) {
        return console.log(err);
      }
      return false;
    });
  });

  ipcMain.on('write_image_setting', (event, args) => {
    const accountPath = path.join(__dirname, `scripts/${args.scriptName}/setting/size.txt`);
    fs.writeFile(accountPath, args.content, function (err) {
      if (err) {
        return console.log(err);
      }
      return false;
    });
  });

  ipcMain.on('write-input', (event, args) => {
    const inputPath = path.join(
      __dirname,
      `scripts/${args.scriptName}/setting/${constants.SCRIPT_NAME.BUY_MA.file.input_csv}`,
    );
    const filePath = args.filePath
      ? args.filePath
      : path.join(__dirname, `scripts/${args.scriptName}/${args.fileName}`);
    var tab = /\t{1,}/g;
    var content = fs
      .readFileSync(filePath)
      .toString()
      .replace(tab, ',');
    var convertContent = content.replace(
      constants.INPUT_CSV_FILE_NO,
      constants.INPUT_CSV_FILE_NO_CONVERT,
    );
    fs.writeFile(inputPath, convertContent, function (err) {
      if (err) {
        return;
      }
    });
    fs.writeFile(filePath, convertContent, function (err) {
      if (err) {
        return;
      }
    });
  });

  ipcMain.on('update-file-upload', (event, args) => {
    var pathFile = null;
    if (args.isSettingFolder === true) {
      pathFile = path.join(__dirname, `scripts/${args.scriptName}/setting/${args.file}`);
    } else {
      pathFile = path.join(__dirname, `scripts/${args.scriptName}/${args.file}`);
    }
    const file = args.path ? args.path : pathFile;
    const [headers] = args.content;
    const header = Object.keys(headers).map(index => {
      return {
        id: index,
        title: index,
      };
    });
    const csvWriter = createCsvWriter({
      path: file,
      header: header,
    });
    csvWriter.writeRecords(args.content).then();
  });

  ipcMain.on('UPDATE_FILE_TEMPLATE_CSV', (event, args) => {
    file = path.join(__dirname, `scripts/${args.scriptName}/setting/${args.file}`);
    const [headers] = args.content;
    const header = Object.keys(headers).map(index => {
      return {
        id: index,
        title: index,
      };
    });
    const csvWriter = createCsvWriter({
      path: file,
      header: header,
    });
    csvWriter.writeRecords(args.content).then(() => {
      const datas = [];
      if (fs.existsSync(file)) {
        fs.createReadStream(file)
          .pipe(csv(args.options))
          .on('data', (row, index) => {
            datas.push(row);
          })
          .on('end', data => {
            if (datas.length > 0) {
              event.reply(args.reply, datas);
            } else {
              event.reply(args.reply, datas);
            }
          });
      } else {
        event.reply(args.reply, datas);
      }
    });
  });

  ipcMain.on('UPDATE_FILE_PRICE', (event, args) => {
    file = path.join(__dirname, `scripts/${args.scriptName}/setting/${args.file}`);
    const [headers] = args.content;
    const header = Object.keys(headers).map(index => {
      return {
        id: index,
        title: index,
      };
    });
    const csvWriter = createCsvWriter({
      path: file,
      header: header,
    });
    csvWriter.writeRecords(args.content).then(() => {
      const datas = [];
      if (fs.existsSync(file)) {
        fs.createReadStream(file)
          .pipe(csv(args.options))
          .on('data', (row, index) => {
            datas.push(row);
          })
          .on('end', data => {
            if (datas.length > 0) {
              event.reply(args.reply, datas);
            } else {
              event.reply(args.reply, datas);
            }
          });
      } else {
        event.reply(args.reply, datas);
      }
    });
  });

  ipcMain.on('write-file', (event, args) => {
    const filePath = path.join(__dirname, `scripts/${args.scriptName}/setting/${args.fileName}`);
    fs.writeFile(filePath, args.content, function (err) {
      if (!err) return false;
    });
  });

  ipcMain.on('write-file-season', (event, args) => {
    const filePath = path.join(__dirname, `scripts/${args.scriptName}/setting/${args.fileName}`);
    var regexEnter = '\n';
    let content = `${constants.SEASON_NOTE}${regexEnter}${args.content}`;
    fs.writeFile(filePath, content, function (err) {
      if (!err) return false;
    });
  });

  ipcMain.on('create-header-footer-size', (event, args) => {
    event.preventDefault();
    event.returnValue = false;
    const filePath = path.join(__dirname, `scripts/${args.scriptName}/setting/${args.fileName}`);
    var regexEnter = '\n\n';
    const content = `${args.header}${regexEnter}${constants.INPUT_SIZE_TXT}${regexEnter}${args.footer}`;
    fs.writeFile(filePath, content, function (err) {
      if (err) {
        return console.log(err);
      }
      return false;
    });
  });

  ipcMain.on('create-header-footer-comment', (event, args) => {
    event.preventDefault();
    event.returnValue = false;
    const filePath = path.join(__dirname, `scripts/${args.scriptName}/setting/${args.fileName}`);
    var regexEnter = '\n\n';
    const content = `${args.header}${regexEnter}${constants.INPUT_COMMENT_TXT}${regexEnter}${args.footer}`;
    fs.writeFile(filePath, content, function (err) {
      if (err) {
        return console.log(err);
      }
      return false;
    });
  });

  ipcMain.on('copy-file', (event, args) => {
    event.preventDefault();
    event.returnValue = false;
    const optFolder = args.optFolder ? args.optFolder : 'setting';
    const newPath = path.join(
      __dirname,
      `scripts/${args.scriptName}/${optFolder}/${args.fileName}`,
    );
    fs.createReadStream(args.file).pipe(fs.createWriteStream(newPath));
  });

  ipcMain.on('copy-file-result', (event, args) => {
    event.preventDefault();
    event.returnValue = false;
    const baseFile = path.join(__dirname, `scripts/${args.baseFile}`);
    const newFile = path.join(__dirname, `scripts/${args.scriptName}/setting/${args.newFile}`);
    try {
      if (fs.existsSync(baseFile)) {
        fs.createReadStream(baseFile).pipe(fs.createWriteStream(newFile));
      }
    } catch (err) { }
  });

  ipcMain.on('copy-file-account', (event, args) => {
    event.preventDefault();
    event.returnValue = false;
    const baseFile = path.join(__dirname, `scripts/${args.file}`);
    const newPath = path.join(__dirname, `scripts/${args.scriptName}/setting/${args.fileName}`);
    try {
      if (fs.existsSync(baseFile)) {
        fsx
          .ensureFile(newPath)
          .then(() => {
            fs.createReadStream(baseFile).pipe(fs.createWriteStream(newPath));
          })
          .catch(err => { });
      }
    } catch (err) { }
  });

  ipcMain.on('COPY_DIR', (event, args) => {
    const baseDir = args.baseDir;
    const newDir = path.join(__dirname, `scripts/${args.scriptName}/${args.newDir}`);
    fsx.emptyDir(newDir).then(() => {
      fsx.copy(baseDir, newDir, err => { });
    });
  });

  ipcMain.on('COPY_DIR_OUT_PUT', (event, args) => {
    const baseDir = path.join(__dirname, `scripts/${args.baseDir}`);
    const newDir = path.join(__dirname, `scripts/${args.scriptName}/${args.newDir}`);
    fsx.emptyDir(newDir).then(() => {
      fsx.copy(baseDir, newDir, err => { });
    });
  });

  ipcMain.on('GET_FIRST_IMAGE_OUT_FRAME', (event, args) => {
    var myPath = path.join(
      __dirname,
      `scripts/${constants.SCRIPT_NAME.ADD_FRAME.name}/${args.folder}/image/*/*`,
    );
    glob(myPath, function (er, files) {
      if (files.length > 0) {
        const [firstImage] = files;
        const file = firstImage.replace(`${__dirname}/`, '');
        event.reply('GET_FIRST_IMAGE_OUT_FRAME_RESULT', file);
      }
    });
  });

  ipcMain.on('read-data-file', (event, args) => {
    event.preventDefault();
    event.returnValue = false;
    const optFolder = args.optFolder ? args.optFolder : 'setting';
    const filePath = path.join(
      __dirname,
      `scripts/${args.scriptName}/${optFolder}/${args.fileName}`,
    );
    try {
      if (fs.existsSync(filePath)) {
        var content = fs
          .readFileSync(filePath)
          .toString()
          .split('\n');
        event.reply(args.reply, content);
      } else {
        event.reply(args.reply, '');
      }
    } catch (err) { }
  });

  ipcMain.on('read-data-file-comment', (event, args) => {
    event.preventDefault();
    event.returnValue = false;
    const optFolder = args.optFolder ? args.optFolder : 'setting';
    const filePath = path.join(
      __dirname,
      `scripts/${args.scriptName}/${optFolder}/${args.fileName}`,
    );
    var content = fs
      .readFileSync(filePath)
      .toString()
      .split('\n');
    const indexInput = content.indexOf(constants.INPUT_COMMENT_TXT);
    const header = content.filter((item, index) => {
      return index < indexInput - 1;
    });
    const footer = content.filter((item, index) => {
      return index > indexInput + 1;
    });
    const data = {
      header: header,
      footer: footer,
    };
    event.reply('read-data-file-comment-reply', data);
  });

  ipcMain.on('read-data-file-size', (event, args) => {
    event.preventDefault();
    event.returnValue = false;
    const optFolder = args.optFolder ? args.optFolder : 'setting';
    const filePath = path.join(
      __dirname,
      `scripts/${args.scriptName}/${optFolder}/${args.fileName}`,
    );
    var content = fs
      .readFileSync(filePath)
      .toString()
      .split('\n');
    const indexInput = content.indexOf(constants.INPUT_SIZE_TXT);
    const header = content.filter((item, index) => {
      return index < indexInput - 1;
    });
    const footer = content.filter((item, index) => {
      return index > indexInput + 1;
    });
    const data = {
      header: header,
      footer: footer,
    };
    event.reply('read-data-file-size-reply', data);
  });
}

ipcMain.on('READ_FILE_CSV', (event, args) => {
  event.preventDefault();
  event.returnValue = false;
  var pathFile = null;
  if (args.isSettingFolder === true) {
    pathFile = path.join(__dirname, `scripts/${args.scriptName}/setting/${args.file}`);
  } else {
    pathFile = path.join(__dirname, `scripts/${args.scriptName}/${args.file}`);
  }
  const file = args.path ? args.path : pathFile;
  const datas = [];
  if (fs.existsSync(file)) {
    var tab = /\t{1,}/g;
    var content = fs
      .readFileSync(file)
      .toString()
      .replace(tab, ',');
    var convertContent = content.replace(
      constants.INPUT_CSV_FILE_NO,
      constants.INPUT_CSV_FILE_NO_CONVERT,
    );
    fs.writeFile(file, convertContent, function (err) {
      if (err) {
        console.log(err);
      }
      fs.createReadStream(file)
        .pipe(csv(args.options))
        .on('data', (row, index) => {
          datas.push(row);
        })
        .on('end', data => {
          if (datas.length > 0) {
            event.reply(args.reply, datas);
          }
        });
    });
  } else {
    event.reply(args.reply, datas);
  }
});

ipcMain.on('READ_FILE_PRICE', (event, args) => {
  event.preventDefault();
  event.returnValue = false;
  pathFile = path.join(__dirname, `scripts/${args.scriptName}/setting/${args.file}`);
  const datas = [];
  if (fs.existsSync(pathFile)) {
    var content = fs.readFileSync(pathFile).toString();
    fs.writeFile(pathFile, content, function (err) {
      if (err) {
        console.log(err);
      }
      fs.createReadStream(pathFile)
        .pipe(csv(args.options))
        .on('data', (row, index) => {
          datas.push(row);
        })
        .on('end', data => {
          if (datas.length > 0) {
            event.reply(args.reply, datas);
          } else {
            event.reply(args.reply, datas);
          }
        });
    });
  } else {
    event.reply(args.reply, datas);
  }
});

ipcMain.on('READ_FILE_TEMPLATE_CSV', (event, args) => {
  event.preventDefault();
  event.returnValue = false;
  file = path.join(__dirname, `scripts/${args.scriptName}/${args.file}`);
  const datas = [];
  if (fs.existsSync(file)) {
    var content = fs
      .readFileSync(file)
      .toString()
    fs.writeFile(file, content, function (err) {
      if (err) {
        return console.log(err);
      }
      fs.createReadStream(file)
        .pipe(csv(args.options))
        .on('data', (row, index) => {
          datas.push(row);
        })
        .on('end', data => {
          if (datas.length > 0) {
            event.reply(args.reply, datas);
          } else {
            event.reply(args.reply, datas);
          }
        });
    });
  } else {
    event.reply(args.reply, datas);
  }
});

ipcMain.on('DELETE_FILE', (event, args) => {
  event.preventDefault();
  event.returnValue = false;
  const optFolder = args.optFolder ? args.optFolder : 'setting';
  args.files.map(file => {
    const filePath = path.join(__dirname, `scripts/${args.scriptName}/${optFolder}/${file}`);
    if (fs.existsSync(filePath)) {
      fs.unlink(filePath, err => {
        if (err) throw err;
      });
    }
  });
});

ipcMain.on('DOWNLOAD_FILE', (event, args) => {
  event.preventDefault();
  event.returnValue = false;
  const filePath = path.join(__dirname, `scripts/${args.scriptName}/${args.fileCopy}`);
  if (fs.existsSync(filePath)) {
    var content = fs
      .readFileSync(filePath)
      .toString()
      .split('\n');
    const data = {
      content: content,
      fileName: args.fileCopy,
    };
    event.reply(args.reply, data);
  } else {
    event.reply(args.reply, '');
  }
});

ipcMain.on('DOWNLOAD_FILE_CSV', (event, args) => {
  event.preventDefault();
  event.returnValue = false;
  const filePath = path.join(__dirname, `scripts/${args.scriptName}/${args.fileCopy}`);
  if (fs.existsSync(filePath)) {
    var content = fs.readFileSync(filePath).toString();
    var datas = [];
    fs.createReadStream(filePath)
      .pipe(csv({}))
      .on('data', (row, index) => {
        datas.push(row);
      })
      .on('end', data => {
        const dateTime = moment().format('YYYYMMDD_HHmmss');
        const filename = `${args.scriptName}_${args.fileCopy}_${dateTime}.csv`
        const response = {
          content: content,
          fileName: filename,
        };
        event.reply(args.reply, response);
      });
  } else {
    event.reply(args.reply, '');
  }
});

ipcMain.on('DELETE_FOLDER', (event, args) => {
  event.preventDefault();
  event.returnValue = false;
  const folderPath = path.join(__dirname, `scripts/${args.scriptName}/${args.folderDelete}`);
  if (fs.existsSync(folderPath)) {
    fs.rmdir(folderPath, { recursive: true }, err => {
      if (err) throw err;
    });
  }
});

ipcMain.on('DELETE_FOLDER_FRAME', (event, args) => {
  event.preventDefault();
  event.returnValue = false;
  const folderPath = path.join(__dirname, `scripts/${args.scriptName}/${args.folderDelete}`);
  fsx.emptyDir(folderPath).catch(err => {
    if (err) throw err;
  });
});

ipcMain.on('ZIP_FOLDER', (event, args) => {
  event.preventDefault();
  event.returnValue = false;
  const folderZip = path.join(__dirname, `scripts/${args.scriptName}/${args.folderZip}`);
  const fileZip = path.join(__dirname, `scripts/${args.scriptName}/${args.folderZip}.zip`);
  zipFolder.zipFolder(folderZip, fileZip, function (err) {
    if (err) {
      event.reply(args.reply, '');
    } else {
      const filePath =
        args.scriptName !== 'buyma'
          ? path.join(__dirname, `scripts/${args.scriptName}/result.csv`)
          : path.join(__dirname, `scripts/${args.scriptName}/setting/input.csv`);
      if (fs.existsSync(filePath)) {
        var content = fs.readFileSync(fileZip);
        var datas = [];
        fs.createReadStream(filePath)
          .pipe(csv({}))
          .on('data', (row, index) => {
            datas.push(row);
          })
          .on('end', data => {
            const dateTime = moment().format('YYYYMMDD_HHmmss');
            if (args.scriptName !== 'buyma') {
              filename =
                datas.length > 0
                  ? `${args.scriptName}_${datas[0].カテゴリ}_${datas[0].ブランド}_${dateTime}`
                  : `${args.scriptName}_${dateTime}`;
            } else {
              filename = `${args.scriptName}_${dateTime}`;
            }
            const response = {
              content: content,
              fileName: filename,
            };
            event.reply(args.reply, response);
          });
      } else {
        var content = fs.readFileSync(fileZip);
        const dateTime = moment().format('YYYYMMDD_HHmmss');
        const filename = `${args.scriptName}_${dateTime}`;
        const response = {
          content: content,
          fileName: filename,
        };
        event.reply(args.reply, response);
      }
    }
  });
});

ipcMain.on('ZIP_FOLDER_OUTPUT', (event, args) => {
  event.preventDefault();
  event.returnValue = false;
  const folderZip = path.join(__dirname, `scripts/${args.scriptName}/${args.folderZip}`);
  const fileZip = path.join(__dirname, `scripts/${args.scriptName}/${args.folderZip}.zip`);
  zipFolder.zipFolder(folderZip, fileZip, function (err) {
    if (err) {
      event.reply(args.reply, '');
    } else {
      var content = fs.readFileSync(fileZip);
      const dateTime = moment().format('YYYYMMDD_HHmmss');
      const filename = `${args.scriptName}_${args.subFolder}_${dateTime}`;
      const response = {
        content: content,
        fileName: filename,
      };
      event.reply(args.reply, response);
    }
  });
});

// ipcMain.on('ADD_MORE_FOLDER', (event, args) => {
//   event.preventDefault();
//   event.returnValue = false
//   const folderCheck = path.join(__dirname, `scripts/${args.scriptName}/${args.folderAdd}`)
//   fs.readdir(folderCheck, function (err, items) {
//     const folderAdd = items.length + 1
//     var image = path.join(__dirname,
//       `scripts/${constants.SCRIPT_NAME.ADD_FRAME.name}/${constants.SCRIPT_NAME.ADD_FRAME.folder.products}/image/*/*`);
//     fs.mkdir(`${folderCheck}/${folderAdd}`, function (err) {
//       glob(image, function (er, files) {
//         if (files.length > 0) {
//           const [firstImage] = files
//           var fileName = path.basename(firstImage);
//           var source = fs.createReadStream(firstImage);
//           var dest = fs.createWriteStream(path.resolve(`${folderCheck}/${folderAdd}`, fileName));
//           source.pipe(dest);
//         }
//       });
//     });
//   })
// })

ipcMain.on('SEARCH_LOG_TXT', (event, args) => {
  const optFolder = args.optFolder ? args.optFolder : 'setting';
  const filePath = path.join(__dirname, `scripts/${args.scriptName}/${optFolder}/${args.file}`);
  if (fs.existsSync(filePath)) {
    var content = fs
      .readFileSync(filePath)
      .toString()
      .trim()
      .split('\n\n');
  }
  event.reply(args.reply, content);
});

ipcMain.on('DELETE_RECORD_IN_LOG_TXT', (event, args) => {
  const optFolder = args.optFolder ? args.optFolder : 'setting';
  const filePath = path.join(__dirname, `scripts/${args.scriptName}/${optFolder}/${args.file}`);
  if (fs.existsSync(filePath)) {
    var content = fs
      .readFileSync(filePath)
      .toString()
      .trim();
    var enterMultiple = /\n{3,}/g;
    var convertContent = content
      .replace(args.content, '\n')
      .replace(enterMultiple, '\n\n')
      .trim();
    fs.writeFile(filePath, convertContent, function test(err) {
      if (err) {
        return;
      } else {
        var fileContent = fs.readFileSync(filePath, 'utf8');
        event.reply(args.reply, fileContent);
      }
    });
  }
});

ipcMain.on('DELETE_ONE_RECORD_IN_LOG_TXT', (event, args) => {
  const optFolder = args.optFolder ? args.optFolder : 'setting';
  const filePath = path.join(__dirname, `scripts/${args.scriptName}/${optFolder}/${args.file}`);
  if (fs.existsSync(filePath)) {
    var content = fs
      .readFileSync(filePath)
      .toString()
      .trim();
    var convertContent = content.replace(args.oldRecord, args.newRecord);
    fs.writeFile(filePath, convertContent, function test(err) {
      if (err) {
        return;
      } else {
        var fileContent = fs.readFileSync(filePath, 'utf8');
        event.reply(args.reply, fileContent);
      }
    });
  }
});

ipcMain.on('UPDATE_DATA_FILE_LOG_TXT', (event, args) => {
  const filePath = path.join(__dirname, `scripts/${args.scriptName}/setting/${args.file}`);
  var newFile = fs.readFileSync(filePath, 'utf8');
  event.reply(args.reply, newFile);
});

ipcMain.on('CHECK_FOLDER_EMPTY', (event, args) => {
  const folderCheck = path.join(__dirname, `scripts/${args.scriptName}/${args.folderCheck}`);
  fsx.ensureDir(folderCheck).then(() => {
    fs.readdir(folderCheck, function (err, items) {
      const isEmptyFolder = items && items.length > 0 ? false : true;
      event.reply(args.reply, isEmptyFolder);
    });
  });
});

ipcMain.on('CHECK_FOLDER_FRAME_EMPTY', (event, args) => {
  const folderCheck = path.join(__dirname, `scripts/${args.scriptName}/${args.folderCheck}`);
  fsx.ensureDir(folderCheck).then(() => {
    fs.readdir(folderCheck, function (err, items) {
      const isEmptyFolder = items && items.length > 0 ? false : true;
      event.reply(args.reply, isEmptyFolder);
    });
  });
});

ipcMain.on('CHECK_FOLDER_PRODUCT_IMAGE_EMPTY', (event, args) => {
  const folderCheck = path.join(__dirname, `scripts/${args.scriptName}/${args.folderCheck}`);
  fsx.ensureDir(folderCheck).then(() => {
    fs.readdir(folderCheck, function (err, items) {
      const isEmptyFolder = items && items.length > 0 ? false : true;
      event.reply(args.reply, isEmptyFolder);
    });
  });
});

ipcMain.on('READ_FILE_ITEM_CHECK_PRICE_CSV', (event, args) => {
  event.preventDefault();
  event.returnValue = false;
  pathFile = path.join(
    __dirname,
    `scripts/${args.scriptName}/setting/${args.subFolder}/${args.file}`,
  );
  const datas = [];
  if (fs.existsSync(pathFile)) {
    var content = fs.readFileSync(pathFile).toString();
    fs.writeFile(pathFile, content, function (err) {
      if (err) {
        console.log(err);
      }
      fs.createReadStream(pathFile)
        .pipe(csv(args.options))
        .on('data', (row, index) => {
          datas.push(row);
        })
        .on('end', data => {
          if (datas.length > 0) {
            event.reply(args.reply, datas);
          } else {
            event.reply(args.reply, datas);
          }
        });
    });
  } else {
    event.reply(args.reply, datas);
  }
});

ipcMain.on('UPDATE_FILE_ITEM_CHECK_PRICE_CSV', (event, args) => {
  pathFile = path.join(
    __dirname,
    `scripts/${args.scriptName}/setting/${args.subFolder}/${args.file}`,
  );
  const [headers] = args.content;
  const header = Object.keys(headers).map(index => {
    return {
      id: index,
      title: index,
    };
  });
  const csvWriter = createCsvWriter({
    path: pathFile,
    header: header,
  });
  csvWriter.writeRecords(args.content).then(() => {
    const datas = [];
    if (fs.existsSync(pathFile)) {
      fs.createReadStream(pathFile)
        .pipe(csv(args.options))
        .on('data', (row, index) => {
          datas.push(row);
        })
        .on('end', data => {
          if (datas.length > 0) {
            event.reply(args.reply, datas);
          } else {
            event.reply(args.reply, datas);
          }
        });
    } else {
      event.reply(args.reply, datas);
    }
  });
});

ipcMain.on('CHECK_FILE_INPUT_CSV', (event, args) => {
  const fileInput = path.join(__dirname, `scripts/${args.scriptName}/setting/${args.file}`);
  if (fs.existsSync(fileInput)) {
    event.reply(args.reply, true);
  } else {
    event.reply(args.reply, false);
  }
});

ipcMain.on('SETTING_PERMISSION_GECKO_DRIVER', (event, geckoDriverFiles) => {
  if (os.platform() === 'darwin') {
    geckoDriverFiles.map(geckoFile => {
      const fileGecko = path.join(__dirname, `scripts/${geckoFile.script}/bin/geckodriver`);
      fs.chmod(fileGecko, 0o777, function (err) {
        if (err) throw err;
      });
    });
  }
});

ipcMain.on('WATCH_FILE', (events, args) => {
  const fileWatch = path.join(__dirname, `scripts/${args.scriptName}/${args.file}`);
  fs.watch(fileWatch, (event, filename) => {
    if (filename && event === 'change') {
      var content = fs.readFileSync(fileWatch).toString();
      events.reply(args.reply, content);
    }
  });
});

ipcMain.on('KILL_PROCESS', (event, args) => {
  let data = [];
  event.preventDefault();
  fixPath();
  event.returnValue = false;
  let cmd = null;
  if (os.platform() === 'darwin') {
    cmd = path.join(__dirname, `scripts/kill_process.sh`);
    if (cmd === null) {
      return;
    }
    data.push(cmd);
    data.push(args.fileBin);
    const ls = spawn('sh', data, { detached: true, windowsHide: true });
    ls.stdout.on('data', data => {
      console.log(`data: ${data}`);
    });
    ls.stderr.on('data', data => {
      console.log(`stderr: ${data}`);
    });
    ls.on('close', () => {
      console.log('finish');
    });
  } else {
    cmd = path.join(__dirname, `scripts/kill_process.bat`);
    data.push(cmd);
    data.push(scriptPid[args.scriptName]);
    const bat = spawn('Call', data, { shell: true });
    bat.stdout.on('data', data => {
      console.log(`data: ${data}`);
    });
    bat.stderr.on('data', data => {
      console.log(`error: ${data}`);
    });
    bat.on('close', () => {
      console.log('finish');
    });
  }
});

ipcMain.on('COPY_IMAGE_FOOTER', (events, args) => {
  const destinationFolderPath = path.join(__dirname, `scripts/${args.destinationFolder}`);
  if (fs.existsSync(destinationFolderPath)) {
    fs.rmdir(destinationFolderPath, { recursive: true }, err => {
      if (err) throw err;
      fsx.ensureDir(destinationFolderPath).then(
        args.listFiles.map((file, index) => {
          fs.copyFile(file, `${destinationFolderPath}/${index + 1}.jpg`, err => {
            if (err) throw err;
          });
        }),
      );
    });
  } else {
    fsx.ensureDir(destinationFolderPath).then(
      args.listFiles.map((file, index) => {
        fs.copyFile(file, `${destinationFolderPath}/${index + 1}.jpg`, err => {
          if (err) throw err;
        });
      }),
    );
  }
});

app.allowRendererProcessReuse = true;
app.on('ready', createWindow);
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});
app.on('activate', () => {
  if (mainWindow === null) {
    createWindow();
  }
});
