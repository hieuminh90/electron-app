instruction_sequence = RubyVM::InstructionSequence.compile_file(ARGV[0])
File.open(ARGV[0].split('.rb').first + '.bin', 'wb+') do |file|
  file.write(instruction_sequence.to_binary)
end