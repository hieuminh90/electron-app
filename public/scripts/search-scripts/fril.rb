
#ファイルパスのフルパスを取得する。
def get_full_path(file_path)
  return File.expand_path(file_path, __dir__)
end
# encoding: UTF-8
ENV["SSL_CERT_FILE"] = get_full_path("./bin/cacert.pem")
Encoding.default_external = Encoding::UTF_8

require 'selenium-webdriver'
require 'csv'
require 'open-uri'
require 'openssl'
require 'bigdecimal'
require 'nkf'
require 'net/http'
require 'uri'
require 'securerandom'
require 'nokogiri'
require 'webdrivers'

begin
  $debug_log = true

  # エラーログを出力する。
  def debug_log(message)
    if $debug_log == true
      log(message, "debug_log.txt")
    end
  end

  # メイン、エラーログ出力メソッドから呼ばれる
  def log(message, file_name)
    log = File.open(get_full_path(file_name), "a")
    log.puts(message)
    log.close()
    puts message
  end

  # メインログを出力する。
  def main_log(message)
    log(message, "main.log")
  end

  # エラーログを出力する。
  def error_log(message)
    log(message, "error.log")
  end

  # main.logとerror.logがあれば削除する。
  def delete_log
    if File.exist?(get_full_path('main.log'))
      File.delete(get_full_path('main.log'))
    end
    if File.exist?(get_full_path('error.log'))
      File.delete(get_full_path('error.log'))
    end
    if File.exist?(get_full_path('debug_log.txt'))
      File.delete(get_full_path('debug_log.txt'))
    end
  end

  #OSチェック
  def os
    @os ||= (
      host_os = RbConfig::CONFIG['host_os']
      case host_os
      when /mswin|msys|mingw|cygwin|bccwin|wince|emc/
        :windows
      when /darwin|mac os/
        :macosx
      when /linux/
        :linux
      when /solaris|bsd/
        :unix
      else
        :unknown
      end
    )
  end

  domain_rakuten = ["https://fril.jp/search/"]
  # https://fril.jp/s?query=raza
  # https://fril.jp/search/%E3%83%9E%E3%82%B9%E3%82%AF

  # get text search
  text_search = []
  if File.exist?(get_full_path("./setting/input_text.txt"))
    File.foreach(get_full_path("./setting/input_text.txt")) do |row|
      text_search << row
    end
  else
    delete_log
    main_log("検索に情報を入力してください。")
    error_log("END")
  end
  url_search = domain_rakuten + text_search
  url_search = [url_search[0] + url_search[1]]
  debug_log("url_search: #{url_search}")
  # chrome_version = "83.0.4103.39"

  def get_chrome_driver
    Webdrivers.install_dir = get_full_path("./bin")
    Webdrivers::Chromedriver.required_version = $chrome_version
    Webdrivers::Chromedriver.update
    # switch OS
    if os == :windows
      Selenium::WebDriver::Chrome.driver_path = get_full_path("./bin/chromedriver.exe")
    elsif os == :macosx
      Selenium::WebDriver::Chrome.driver_path = get_full_path("./bin/chromedriver")
    end
    client = Selenium::WebDriver::Remote::Http::Default.new
    client.read_timeout = 180
    options = Selenium::WebDriver::Chrome::Options.new
    options.add_argument('--headless')
    options.add_argument('--disable-gpu')
    options.add_argument('--window-size=2736,1824')
    # エラー以上のログを出力します。
    options.add_argument('--log-level=2')
    d = Selenium::WebDriver.for :chrome, options: options, http_client: client
    wait = Selenium::WebDriver::Wait.new(:timeout => 10)
    return d
  end

  delete_log
  File.open(get_full_path("status.txt"), "w") 
  # header csv
  bom = %w(EF BB BF).map { |e| e.hex.chr }.join
  csv_file = CSV.generate(bom) do |csv|
    csv << ["取得日", "取得順位", "商品名", "URL", "ショップ名", "会社名", "住所", "電話番号", "メールアドレス", "担当者名", "ブランド"
    ]
  end

  x = 1
  if x == 1
    # result.csv に上書きでヘッダを書き込む。
    File.open(get_full_path("result.csv"), "w") do |file|
      file.write(csv_file)
    end
  end

  def get_item_urls(url_search)
    d = get_chrome_driver()
    item_urls = []
    w = 0
    url_search.each do |url|
      begin
        d.get(url)
      rescue
        begin
          d.quit
        rescue
        end
        d = get_chrome_driver()
        retry
      end

      x = 0
      loop do
        # x += 1
        # break if x > 200
        debug_log("loop get item urls 1: #{x}")
        
        # item_urls.uniq.each_with_index do |page, index|

        d.find_elements(:class, 'item-box__item-name').each do |item|
          debug_log("loop get item urls 2: #{x + 1}")
          x = x + 1
          break if x > 100
          item_urls << item.find_element(:tag_name, 'a').attribute("href")
        end
    
        sleep 1
        if d.find_elements(:class, 'pagination').size > 0
          if d.find_element(:class, 'pagination').find_elements(:class, 'last').size > 0
            current_page = d.find_element(:class, 'pagination').find_element(:xpath, '//*[@class="page current"]').text
            begin
              d.find_element(:class, 'pagination').find_element(:link_text, (current_page.to_i + 1)).click
            rescue
              d.navigate.refresh
              retry
            end
          else
            break
          end
          else
            break
          end
          x = x + 1
          break if x > 100
        end
        # x += 1
        # break if x > 200
      end
      begin
        d.quit
      rescue
    end
    return item_urls
  end

  $opt = {}
  $opt['User-Agent'] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
  retry_count = 0
  begin
    item_urls = get_item_urls(url_search)
    total_item_urls_count = item_urls.length
    main_log("total_item_urls_count: " + total_item_urls_count.to_s)
    item_urls.uniq.each_with_index do |page, index|
      main_log(index + 1)
          debug_log(page.to_s)
          doc = Nokogiri::HTML(open(page, $opt))

          # shop name (ショップ名)
          name_shop = ""
          if doc.xpath('//*[@class="row header-shopinfo shopinfo-area"]').size > 0 || doc.xpath('//*[@class="header-shopinfo__shop-name clearfix"]').size > 0
            debug_log("Name shop: #{doc.xpath('//*[@class="header-shopinfo__shop-name clearfix"]').text}")
            name_shop = doc.xpath('//*[@class="header-shopinfo__shop-name clearfix"]').text
          # else
            # name_shop = ""
          end
          
          # product name (ブランド)
          product_name = ""
          if doc.xpath('//*[@class="item__details"]').text.include?(" ブランド")
            debug_log("product name")
            if product_name = doc.xpath('//*[@class="content-group__title"]').text.include?("同一ブランドの商品")
              product_name = doc.xpath('//*[@class="relation-list"]').text.split("\n")[1].tr("\n", "").tr(" ", "")
            else
              product_name = doc.xpath('//*[@class="item__details"]').css('tr')[2].css('td').text.gsub("\n", "").tr(" ","")
            end
          end

          # title name (商品名)
          title_name = ""
          if doc.xpath('//*[@class="item-info row"]').size > 0 || doc.xpath('//*[@class="item-info__header"]').size > 0
            debug_log("Title name: #{doc.xpath('//*[@class="item__name"]').text}")
            title_name = doc.xpath('//*[@class="item__name"]').text
          else
            title_name = ""
          end
    
          # owner (担当者名)
          owner = ""
          if doc.xpath('//*[@class="row header-shopinfo shopinfo-area"]').size > 0 || doc.xpath('//*[@class="header-shopinfo__user-name"]').size > 0
            debug_log("Owner: #{doc.xpath('//*[@class="header-shopinfo__user-name"]').text}")
            owner = doc.xpath('//*[@class="header-shopinfo__user-name"]').text
          else
            owner = ""
          end
          today = Time.new.strftime("%Y%m%d") 
          # x = 1
        CSV.open(get_full_path("result.csv"), "a") do |file|
          File.open(get_full_path("status.txt"), "w") {|f| f.puts "#{item_urls.size}/#{index +1}"}
          # "取得日", "取得順位", "商品名", "URL", "ショップ名", "会社名", "住所", "電話番号", "メールアドレス", "担当者名", "発送元の地域", "ブランド"
          file << [today, index +1, title_name, page, name_shop, nil, nil, nil, nil, owner, product_name
          ]
        end
    end
    main_log("END")
    # 残り抽出可能商品数を返却する。
    # 処理を終了する。
    exit
  rescue => error
    error_log("エラーが発生しました。エラー内容")
    main_log("extract scraping error occuerd !")
    if error != nil
      error_log("error.message:" + error.message)
      error_log(error.backtrace)
    end
    if retry_count > 10
      #next
      error_log("@ retry count over 10 !")
      $item_urls_count -= 1
      return ""
    else
      #sleep 10
      retry_count += 1
      sleep (10 + (retry_count * 5))
      error_log("@ retry scraping:" + page)
      retry
    end
    

  end
rescue
  main_log("extract error!")
  error_log("エラーが発生しました。エラー内容")
  error_log("END")
end