module.exports = {
	SCRIPT_NAME: {
		SEARCH_SCRIPT: {
			name: 'search-scripts',
			file: {
				comment: 'comment.txt',
				account: 'account.txt',
				size: 'size_info.txt',
				input_csv: 'input.csv',
				error_log: 'error.log',
				error_csv: 'error.csv'
			},
			folder: {
				image: 'image'
			}
		},
	},
	FILE_ERROR_LOG: 'error.log',
	INPUT_COMMENT_TXT: '{input.csvの商品コメントが入ります。消さないでください。}',
	INPUT_SIZE_TXT: '{input.csvのサイズ補足説明が入ります。消さないでください。}',
	INPUT_CSV_FILE_NO: 'ファイルNo.',
	INPUT_CSV_FILE_NO_CONVERT: 'ファイルNo',
	SEASON_NOTE: '新シーズンのみ抽出する際は「1」をすべてを抽出する際は「0」を入力してください。',
	FILE_COMPILER_RUBY: 'compiler.rb',
	FILE_EXEC_RUBY: 'exec.rb'
}
