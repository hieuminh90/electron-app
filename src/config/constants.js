module.exports = {
	SCRIPT_NAME: {
		SEARCH_SCRIPT: {
			name: 'search-scripts',
			file: {
				main_log: 'main.log',
				error_log: 'error.log',
				error_csv: 'error.csv',
				result_csv: 'result.csv',
				input_text: 'input_text.txt',
				log_txt: 'log.txt',
				available_item: 'available_item_count.txt',
				amazon_ruby: 'amazon.rb',
				fril_ruby: 'fril.rb',
				rakuten_ruby: 'rakuten_ruby.rb',
				status: 'status.txt',
			},
			folder: {
				image: 'image',
				image_footer: 'image_footer'
			},
			content_main_log_report: '残り抽出可能商品数：'
		}
	},
	SCRIPT_ID: {
		BUY_MA: 1,
		ADD_FRAME: 2,
		ASOS: 3,
		FARFETCH: 4,
		MATCHES_FASHION: 5,
		LOUIS_VUITTON: 6,
		ITEM_CHECK: 7
	},
	COLUME_HIDDEN: [
		'画像1',
		'画像2',
		'画像3',
		'画像4',
		'画像5',
		'画像6',
		'画像7',
		'画像8',
		'画像9',
		'画像10',
		'画像11',
		'画像12',
		'画像13',
		'画像14',
		'画像15',
		'画像16',
		'画像17',
		'画像18',
		'画像19',
		'画像20',
	],
	SCRIPT_TAB_KEY: {
		BUY_MA: '1',
		ADD_FRAME: '2',
		ASOS: '3',
		FARFETCH: '4',
		MATCHES_FASHION: '5',
		LOUIS_VUITTON: '6',
		STOCK_CHECK: '7',
		CRAWL_DATA: '8',
	},
	COLUME_LARGE: [
		'カテゴリ',
		'ブランド',
		'商品名',
		'色・サイズ補足情報',
		'商品コメント',
		'出品メモ',
		'URL'
	],
	GECKO_DRIVER_FILES: [
		{
			'script': 'louisvuitton'
		},
		{
			'script': 'matchesfashion'
		}
	],
	TEMPLATE_COLUME_HIDDEN: [
		'モデル',
		'色',
		'サイズ',
		'連絡事項',
		'商品コメント',
		'色・サイズ補足情報',
	],
	TEMPLATE_COLUME_AUTOCOMPLETE: [
		{
			'name': 'カテゴリ',
			'key': 'category'
		},
		{
			'name': 'ブランド',
			'key': 'brand'
		},

		{
			'name': 'シーズン',
			'key': 'season'
		},

		{
			'name': 'テーマ',
			'key': 'theme'
		},
		{
			'name': '買付地海外/国内',
			'key': 'purchase_location'
		},
		{
			'name': '買付エリア',
			'key': 'purchase_area'
		},
		{
			'name': '発送地海外/国内',
			'key': 'shipping_location'
		},
		{
			'name': '発送エリア',
			'key': 'shipping_area'
		},
		{
			'name': '発送エリア2',
			'key': 'shipping_area_2'
		},
		{
			'name': '出品ステータス',
			'key': 'listing_status'
		},
		{
			'name': '関税',
			'key': 'customs_duty'
		},
	],
	PATTERN_PRICE_CSV: [
		{
			為替: "",
			倍率: "",
			海外送料: "",
			国内送料: "",
			関税率: "",
			key: "0"
		}
	],
	PATTERN_TEMPLATE_CSV: [
		{
			カテゴリ: "レディースファッション > ワンピース・オールインワン > ワンピース",
			ブランド: "Chi Chi London(チチロンドン)",
			モデル: "指定なし",
			タイトルヘッダー: "送関込☆$BRAND☆",
			色: "色",
			サイズ: "サイズ",
			連絡事項: "常にチェックをつける",
			シーズン: "指定なし",
			タグ: "日本未入荷",
			テーマ: "円高還元セール特集！",
			配送方法: "日本郵便/ゆうパック/0/あり/14-25↵速達便/日本郵便ゆうパック/4000/あり/7-14",
			数量: "1",
			購入期限: "30",
			'買付地海外/ 国内': "ヨーロッパ",
			買付エリア: "イギリス",
			'発送地海外 / 国内': "国内",
			発送エリア: "東京都",
			発送エリア2: "アジア",
			出品ステータス: "go",
			関税: "関税元払い",
			商品コメント: "↵↵ ↵ $COMMENT↵ ↵↵",
			'色・サイズ補足情報': "↵↵↵ $COMMENT↵ ↵↵",
			key: "0"
		}
	]
}
