import React from "react";
import { withTranslation } from 'react-i18next';
import "antd/dist/antd.css";
import { Table, Input, Form, Popconfirm, Icon, AutoComplete, Spin, Select } from "antd";
import { TEMPLATE_COLUME_HIDDEN, TEMPLATE_COLUME_AUTOCOMPLETE } from '../config/constants';
import ScriptService from '../services/script.service';
import { spawnAlert } from '../util/common';
const EditableContext = React.createContext();
const { TextArea } = Input;
const EditableRow = ({ form, index, ...props }) => (
    <EditableContext.Provider value={form}>
        <tr {...props} />
    </EditableContext.Provider>
);
const EditableFormRow = Form.create()(EditableRow);

class EditableCell extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            dataSource: [],
            editing: false,
            typingTimer: '',
            notFound: ''
        };
    }

    toggleEdit = () => {
        const editing = !this.state.editing;
        this.setState({ editing }, () => {
            if (editing) {
                this.input.focus();
            }
        });
    };

    save = e => {
        const { record, handleSave } = this.props;
        this.form.validateFields((error, values) => {
            if (error && error[e.currentTarget.id]) {
                return;
            }
            this.toggleEdit();
            handleSave({ ...record, ...values });
        });
    };

    handleSearch = (value, type) => {
        var data = {}
        if (value) {
            data = {
                type: type,
                description: value
            }
        } else {
            data = {
                type: type,
            }
        }
        ScriptService.autocompleteTemplate(data).then((res) => {
            if (res) {
                if (res.length > 0) {
                    this.setState({
                        notFound: '',
                        dataSource: res.filter(value => value)
                    })
                } else {
                    this.setState({
                        notFound: '対象が見つかりません。',
                        dataSource: res.filter(value => value)
                    })
                }
            }
        }).catch(err => {
            spawnAlert('エラー', 'サーバーへの接続が失敗したので、アプリを再起動してください', 'error');
            this.setState({
                notFound: ''
            })
        })
    };

    handleSearchTmp = (value, type) => {
        this.setState({
            dataSource: [],
            notFound: <Spin />
        }, () => {
            const { typingTimer } = this.state;
            clearTimeout(typingTimer)
            let typingTimerTmp = setTimeout(() => {
                this.handleSearch(value, type)
            }, 2000)
            this.setState({ typingTimer: typingTimerTmp })
        })
    }

    onSelect = (e) => {
        const { record, handleSave } = this.props;
        this.form.validateFields((error, values) => {
            if (error && error[e.currentTarget.id]) {
                return;
            }
            this.toggleEdit();
            handleSave({ ...record, ...values });
        });
    }

    renderCell = form => {
        this.form = form;
        const {
            children,
            dataIndex,
            record,
            autocomplete,
            type,
        } = this.props;
        const { editing } = this.state;
        if (autocomplete) {
            const { dataSource } = this.state;
            return editing ? (
                <Form.Item style={{ margin: 0 }} >
                    {form.getFieldDecorator(dataIndex, {
                        initialValue: record[dataIndex],
                    })(
                        <AutoComplete
                            dataSource={dataSource}
                            dropdownMatchSelectWidth={false}
                            onSelect={this.onSelect}
                            allowClear={true}
                            backfill={true}
                            onSearch={(value) => this.handleSearchTmp(value, type)}
                            onFocus={(value) => this.handleSearchTmp(value, type)}
                            notFoundContent={this.state.notFound}
                        >
                            <TextArea style={{ minHeight: 220, background: '#fff', margin: 0 }}
                                rows={10} ref={node => (this.input = node)} onBlur={this.save}
                            />
                        </AutoComplete>
                    )}
                </Form.Item>
            ) : (
                    <div
                        className="editable-cell-value-wrap"
                        onClick={this.toggleEdit}
                    >
                        {children}
                    </div>
                );
        } else {
                return editing ? (
                    <Form.Item style={{ margin: 0 }} >
                        {form.getFieldDecorator(dataIndex, {
                            initialValue: record[dataIndex],
                        })(
                            <TextArea style={{ minHeight: 150 }}
                                rows={10} ref={node => (this.input = node)}
                                onBlur={this.save} />
                        )}
                    </Form.Item>
                ) : (
                        <div
                            className="editable-cell-value-wrap"
                            onClick={this.toggleEdit}
                        >
                            {children}
                        </div>
                    );
        }
    };
    render() {
        const {
            editable,
            dataIndex,
            title,
            record,
            index,
            handleSave,
            children,
            autocomplete,
            ...restProps
        } = this.props;
        return (
            <td {...restProps} style={{ padding: 0 }} className="cell-table">
                {editable ? (
                    <EditableContext.Consumer>{this.renderCell}</EditableContext.Consumer>
                ) : (
                        children
                    )}
            </td>
        );
    }
}
class TemplateSettingTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            editingKey: '',
            data: null,
            columns: [],
            dataTable: []
        }
    }

    componentWillReceiveProps(newProps) {
        if (typeof newProps.dataTable === 'undefined') return
        if (newProps.dataTable.length > 0) {
            this.setState({ dataTable: newProps.dataTable }, () => {
                this.handleData()
            })
        } else if (newProps.dataTable.length === 0 && this.props.dataTable.length > 0) {
            this.setState({ dataTable: [] }, () => {
                this.handleData()
            })
        }
    }

    handleData() {
        const { dataTable } = this.state;
        if (dataTable.length === 0) return
        if (dataTable.length > 0) {
            const options = {};
            const isHeader = (typeof options.headers === 'undefined' || options.headers) ? true : false;
            const [headers] = dataTable;
            delete headers.key
            var allColums = Object.keys(headers)
            var columnsShow = allColums.filter(data => !TEMPLATE_COLUME_HIDDEN.includes(data))
            var columns = columnsShow.map((index) => {
                let found = TEMPLATE_COLUME_AUTOCOMPLETE.findIndex((value) => value.name === index);
                if (found >= 0) {
                    return {
                        title: (isHeader) ? index : '',
                        dataIndex: index,
                        width: 250,
                        editable: true,
                        align: 'center',
                        autocomplete: true,
                        type: TEMPLATE_COLUME_AUTOCOMPLETE[found]['key'],
                    }
                } else {
                    return {
                        title: (isHeader) ? index : '',
                        dataIndex: index,
                        width: 250,
                        editable: true,
                        align: 'center',
                        autocomplete: false
                    }
                }
            })
        }

        const editableColumm = {
            title: this.props.t('stock_check.action'),
            width: 120,
            dataIndex: 'operation',
            align: 'center',
            className: 'action-colume',
            render: (text, record) => {
                return (
                    <div style={{ marginTop: 70, marginBottom: 70, textAlign: "center" }}>
                        <Popconfirm
                            title={this.props.t('button.delete_confirm')}
                            onConfirm={() => this.handleDelete(record.key)}
                            okText={this.props.t('button.confirm')}
                            cancelText={this.props.t('button.cancel')}
                        >
                            <a href="nothing"><Icon type="delete" style={{ fontSize: 20, color: 'red' }} /></a>
                        </Popconfirm>
                    </div>
                );
            },
        };
        columns.unshift(editableColumm);
        this.setState({
            columns: columns,
            data: Object.keys(dataTable).map((index) => {
                const item = dataTable[index];
                item.key = index;
                return item;
            }),
        });
    }

    handleDelete = key => {
        const data = [...this.state.data];
        const revertData = data.filter(item => item.key !== key)
        if (revertData.length > 0) {
            this.setState({
                data: revertData,
            });
            this.props.getTableData(revertData);
        } else {
            this.props.getTableData(revertData);
        }
    };

    edit(key) {
        this.setState({ editingKey: key });
    }

    handleSave = row => {
        const newData = [...this.state.data];
        const index = newData.findIndex(item => row.key === item.key);
        if (index > -1) {
            const item = newData[index];
            newData.splice(index, 1, {
                ...item,
                ...row,
            });
        } else {
            newData.push(row);
        }
        this.props.getTableData(newData);
        this.setState({ data: newData });
    };

    render() {
        const components = {
            body: {
                row: EditableFormRow,
                cell: EditableCell
            },
        };
        const columns = this.state.columns.map(col => {
            if (!col.editable) {
                return col;
            }
            return {
                ...col,
                onCell: record => ({
                    record,
                    editable: col.editable,
                    dataIndex: col.dataIndex,
                    title: col.title,
                    type: col.type,
                    autocomplete: col.autocomplete,
                    handleSave: this.handleSave,
                })
            };
        });
        return (
            <div>
                <EditableContext.Provider value={this.props.form}>
                    <Table
                        components={components}
                        bordered
                        scroll={{ x: 'calc(700px + 50%)', y: 800 }}
                        columns={columns}
                        dataSource={this.state.data}
                        pagination={false}
                    />
                </EditableContext.Provider>
            </div>
        );
    }
}

const TemplateSettingTableForm = Form.create()(TemplateSettingTable);
export default withTranslation('common')(TemplateSettingTableForm);
