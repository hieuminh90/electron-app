import React from 'react';
import 'antd/dist/antd.css';
import { Upload, Icon } from 'antd';
const { Dragger } = Upload;
class DropMultiFile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isErrorImageFooter: false
    }
  }
 
  beforeUpload = (file, fileList) => {
    let isError = false
    if (fileList && fileList.length > 10) {
      isError = true
      this.setState({
        isErrorImageFooter: true
      })
      this.props.changeFile(isError, []);
      return;
    } else {
      this.setState({
        isErrorImageFooter: false
      })
      this.props.changeFile(isError, fileList);
    }
    return false;
  };
  render() {
    const { name, fileType } = this.props;
    const props = {
      multiple: true,
      id: name,
      accept: fileType,
      showUploadList: false,
      beforeUpload: this.beforeUpload,
    };
    return (
      <Dragger {...props} style={{borderColor: this.state.isErrorImageFooter ? 'red' : ''}}>
        <p className="ant-upload-drag-icon">
          <Icon style={{ fontSize: 30 }} type="inbox" />
        </p>
        <p style={{ color: 'rgba(0, 0, 0, 0.65)', fontSize: 13 }} className="ant-upload-text">
          クリックしてファイルを選択 もしくは ここにドラッグ＆ドロップ
        </p>
      </Dragger>
    );
  }
}
export default DropMultiFile;
