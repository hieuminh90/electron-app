import React from "react";
import "antd/dist/antd.css";
import { Table, Input, Form, Popconfirm, Icon, Divider } from "antd";
import { withTranslation } from 'react-i18next';
import '../components/Buyma/buyma.css';
const EditableContext = React.createContext();
const { TextArea } = Input;
class TableLogTxtComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            editingKey: '',
            data: null,
            columns: [],
            dataTable: []
        }
    }

    componentWillReceiveProps(newProps) {
        if (newProps.logData.length > 0) {
            this.setState({ dataTable: newProps.logData }, () => {
                this.handleData()
            })
        } else if (newProps.logData.length === 0 && this.props.logData.length > 0) {
            this.setState({ dataTable: [] }, () => {
                this.handleData()
            })
        }
    }

    handleData() {
        const { dataTable } = this.state;
        var columns = [
            {
                title: this.props.t('buyma.exhibition_date'),
                dataIndex: 'dateTime',
                key: this.props.t('buyma.exhibition_date'),
                width: 250,
                align: 'center',
                actionColume: false,
                editable: true,
            },
            {
                title: this.props.t('buyma.product_URL'),
                dataIndex: 'url',
                key: this.props.t('buyma.product_URL'),
                width: 1000,
                align: 'center',
                actionColume: false,
                editable: true,
            },
        ];

        const editableColumm = {
            title: this.props.t('stock_check.action'),
            width: 120,
            dataIndex: 'operation',
            align: 'center',
            className: 'action-colume',
            actionColume: true,
            render: (text, record) => {
                const { editingKey } = this.state;
                const editable = this.isEditing(record);
                return editable ? (
                    <div style={{ textAlign: "center" }}>
                        <span style={{ textAlign: "center" }} >
                            <EditableContext.Consumer >
                                {form => (
                                    <a href="nothing" onClick={() => this.save(form, record)} >
                                        <Icon type="check" style={{ fontSize: 18 }} />
                                    </a>
                                )}
                            </EditableContext.Consumer>
                            <Divider type="vertical" style={{ fontSize: 30, }} />
                            <Popconfirm
                                title={this.props.t('button.cancel_confirm')}
                                onConfirm={() => this.cancel(record.key)}
                                okText={this.props.t('button.confirm')}
                                cancelText={this.props.t('button.cancel')}
                            >
                                <a href="nothing" ><Icon type="close" style={{ fontSize: 18, color: 'red' }} /></a>
                            </Popconfirm>
                        </span>
                    </div>
                ) : (
                        <div style={{ textAlign: "center" }}>
                            <a href="nothing" disabled={editingKey !== ''} onClick={() => this.edit(record)}>
                                <Icon type="edit" style={{ fontSize: 20, }} />
                            </a>
                            <Divider type="vertical" style={{ fontSize: 30, marginRight: 10 }} />
                            <Popconfirm
                                title={this.props.t('button.delete_confirm')}
                                onConfirm={() => this.handleDelete(record)}
                                okText={this.props.t('button.confirm')}
                                cancelText={this.props.t('button.cancel')}
                            >
                                {(editingKey === '') ? 
                                (<a href="nothing" ><Icon type="delete"  style={{ fontSize: 20, color: 'red' }} /></a>): (
                                    <a href="nothing" disabled={true}  ><Icon type="delete"  style={{ fontSize: 20, color: '#80808091' }} /></a>
                                )}
                                
                            </Popconfirm>

                        </div>
                    );
            }
        };
        columns.unshift(editableColumm);
        this.setState({
            columns: columns,
            data: (dataTable.length > 0) ? Object.keys(dataTable).map((index) => {
                const item = dataTable[index];
                item.key = index;
                return item;
            }) : [],
        });
    }

    handleDelete = (record) => {
        const data = [...this.state.data];
        const revertData = data.filter(item => item.key !== record.key)
        this.setState({
            data: revertData,
        });
        this.props.getTableData(record);
    };

    isEditing = record => record.key === this.state.editingKey;

    cancel = () => {
        this.setState({ editingKey: '' });
    };

    save(form, record) {
        const key = record.key
        form.validateFields((error, row) => {
            if (error) {
                return;
            }
            const newData = [...this.state.data];
            const index = newData.findIndex(item => key === item.key);
            if (index > -1) {
                const item = newData[index];
                newData.splice(index, 1, {
                    ...item,
                    ...row,
                });
            } else {
                newData.push(row);
            }
            this.setState({ data: newData, editingKey: '' }, () => {
                this.props.getTableData(record, row);
            });
        });
    }

    edit(record) {
        this.setState({ editingKey: record.key });
    }

    render() {
        const components = {
            body: {
                cell: EditableCell
            },
        };
        const columns = this.state.columns.map(col => {
            return {
                ...col,
                onCell: record => ({
                    record,
                    dataIndex: col.dataIndex,
                    title: col.title,
                    editing: this.isEditing(record),
                    isActionColume: col.actionColume
                })
            };
        });
        const { data } = this.state;
        return (
            <div>
                <EditableContext.Provider value={this.props.form}>
                    <Table
                        components={components}
                        bordered
                        scroll={{ x: 'calc(700px + 50%)', y: 500 }}
                        columns={columns}
                        dataSource={data}
                        pagination={false}
                    />
                </EditableContext.Provider>
            </div>
        );
    }
}

class EditableCell extends React.Component {
    renderCell = ({ getFieldDecorator }) => {
        const {
            editing,
            dataIndex,
            title,
            record,
            index,
            children,
            isActionColume,
            ...restProps
        } = this.props;
        return (
            isActionColume ? (
                <td {...restProps} style={{ padding: 5 }}>
                    {children}
                </td>
            ) : (
                    <td {...restProps} style={{ padding: 0 }}>
                        {editing ? (
                            <Form.Item style={{ margin: 0 }}>
                                {getFieldDecorator(dataIndex, {
                                    initialValue: record[dataIndex],
                                })(<TextArea
                                    rows={10}
                                    value={children[2]}
                                />)}
                            </Form.Item>
                        ) : (
                                <TextArea
                                    rows={10}
                                    value={children[2]}
                                    style={{ background: 'inherit', color: 'rgba(0, 0, 0, 0.65)', border: 'unset' }}
                                    disabled={true} />
                            )
                        }
                    </td>
                )
        );
    };
    render() {
        return <EditableContext.Consumer>{this.renderCell}</EditableContext.Consumer>;
    }
}

const TableLogTxtComponenttForm = Form.create()(TableLogTxtComponent);
export default withTranslation('common')(TableLogTxtComponenttForm);
