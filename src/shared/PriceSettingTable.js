import React from "react";
import "antd/dist/antd.css";
import { Table, Form, Popconfirm, Icon, InputNumber } from "antd";
import { withTranslation } from 'react-i18next';
const EditableContext = React.createContext();
const EditableRow = ({ form, index, ...props }) => (
    <EditableContext.Provider value={form}>
        <tr {...props} />
    </EditableContext.Provider>
);
const EditableFormRow = Form.create()(EditableRow);

class EditableCell extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            editing: false,
        };
    }

    toggleEdit = () => {
        const editing = !this.state.editing;
        this.setState({ editing }, () => {
            if (editing) {
                this.input.focus();
            }
        });
    };

    save = e => {
        const { record, handleSave } = this.props;
        this.form.validateFields((error, values) => {
            if (error && error[e.currentTarget.id]) {
                return;
            }
            this.toggleEdit();
            handleSave({ ...record, ...values });
        });
    };

    renderCell = form => {
        this.form = form;
        const {
            dataIndex,
            record,
            children,
        } = this.props;
        const { editing } = this.state;
        return editing ? (
            <Form.Item style={{ margin: 0 }}>
                {form.getFieldDecorator(dataIndex, {
                    initialValue: record[dataIndex],
                })(<InputNumber
                    max={999999999}
                    min={0}
                    ref={node => (this.input = node)}
                    onBlur={this.save}
                    style={{ width: '100%', height: 50 }} />)}
            </Form.Item>
        ) : (
                <div
                    className="editable-cell-value-wrap"
                    onClick={this.toggleEdit}
                >
                    {children}
                </div>
            )
    };
    render() {
        const {
            editable,
            dataIndex,
            title,
            record,
            index,
            handleSave,
            children,
            ...restProps
        } = this.props;
        return (
            <td {...restProps} style={{ padding: 0, height: 80 }} className="cell-table">
                {editable ? (
                    <EditableContext.Consumer>{this.renderCell}</EditableContext.Consumer>
                ) : (
                        children
                    )}
            </td>
        );
    }
}
class PriceSettingTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: null,
            columns: [],
            dataTable: []
        }
    }

    componentWillReceiveProps(newProps) {
        if (newProps.dataTable.length > 0) {
            this.setState({ dataTable: newProps.dataTable }, () => {
                this.handleData()
            })
        } else if (newProps.dataTable.length === 0 && this.props.dataTable.length > 0) {
            this.setState({ dataTable: [] }, () => {
                this.handleData()
            })
        }
    }

    handleData() {
        const { dataTable } = this.state;
        if (dataTable.length === 0) return
        if (dataTable.length > 0) {
            const options = {};
            const isHeader = (typeof options.headers === 'undefined' || options.headers) ? true : false;
            const [headers] = dataTable;
            delete headers.key
            var columns = Object.keys(headers).map((index) => {
                return {
                    title: (isHeader) ? index : '',
                    dataIndex: index,
                    width: 150,
                    editable: true,
                    align: 'center',
                }
            })
        }

        const editableColumm = {
            title: this.props.t('stock_check.action'),
            width: '25%',
            dataIndex: 'operation',
            align: 'center',
            className: 'action-colume',
            render: (text, record) => {
                return (
                    <div style={{ textAlign: "center" }}>
                        <Popconfirm
                            title={this.props.t('button.delete_confirm')}
                            onConfirm={() => this.handleDelete(record.key)}
                            okText={this.props.t('button.confirm')}
                            cancelText={this.props.t('button.cancel')}
                        >
                            <a href="nothing" ><Icon type="delete" style={{ fontSize: 20, color: 'red' }} /></a>
                        </Popconfirm>
                    </div>
                );
            },
        };
        columns.unshift(editableColumm);
        this.setState({
            columns: columns,
            data: Object.keys(dataTable).map((index) => {
                const item = dataTable[index];
                item.key = index;
                return item;
            }),
        });
    }

    handleDelete = key => {
        const data = [...this.state.data];
        const revertData = data.filter(item => item.key !== key)
        if (revertData.length > 0) {
            this.setState({
                data: revertData,
            });
            this.props.getTableData(revertData);
        } else {
            this.props.getTableData(revertData);
        }
    };

    edit(key) {
        this.setState({ editingKey: key });
    }

    handleSave = row => {
        const newData = [...this.state.data];
        const index = newData.findIndex(item => row.key === item.key);
        if (index > -1) {
            const item = newData[index];
            newData.splice(index, 1, {
                ...item,
                ...row,
            });
        } else {
            newData.push(row);
        }
        this.props.getTableData(newData);
        this.setState({ data: newData });
    };

    render() {
        const components = {
            body: {
                row: EditableFormRow,
                cell: EditableCell
            },
        };
        const columns = this.state.columns.map(col => {
            if (!col.editable) {
                return col;
            }
            return {
                ...col,
                onCell: record => ({
                    record,
                    editable: col.editable,
                    dataIndex: col.dataIndex,
                    title: col.title,
                    type: col.type,
                    handleSave: this.handleSave,
                })
            };
        });
        return (
            <div>
                <EditableContext.Provider value={this.props.form}>
                    <Table
                        components={components}
                        bordered
                        columns={columns}
                        dataSource={this.state.data}
                        pagination={false}
                    />
                </EditableContext.Provider>
            </div>
        );
    }
}

const PriceSettingTableForm = Form.create()(PriceSettingTable);
export default withTranslation('common')(PriceSettingTableForm);
