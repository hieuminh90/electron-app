import React from 'react';
import 'antd/dist/antd.css';
import { withTranslation } from 'react-i18next';
import { Upload, Icon } from 'antd';
const Upath = require('upath');
const { Dragger } = Upload;
class DropFolder extends React.Component {
  beforeUpload = (file, fileList) => {
    var firstFile = fileList[0]
    if (typeof firstFile !== 'undefined') {
      const paths = firstFile.webkitRelativePath.split("/");
      const [firstPart] = paths;
      const path = Upath.normalize(firstFile.path)
      const newPath = `${path.replace(firstFile.webkitRelativePath, '')}${firstPart}`;
      const finishPath = Upath.normalize(newPath)
      this.props.handleChangeFolder(finishPath);
      return false;
    } else {
      this.props.handleChangeFolder('');
    }
    return false
  }
  render() {
    const { name } = this.props;
    const props = {
      id: name,
      directory: true,
      showUploadList: false,
      multiple: true,
      beforeUpload: this.beforeUpload
    };
    return (
      <Dragger {...props}>
      <p className="ant-upload-drag-icon">
        <Icon style={{ fontSize: 30}} type="inbox" />
      </p>
      <p style={{color : 'rgba(0, 0, 0, 0.65)' , fontSize: 13}} className="ant-upload-text">クリックしてファイルを選択 もしくは ここにドラッグ＆ドロップ</p>
    </Dragger>
    )
  }
}
export default withTranslation('common')(DropFolder);
