import React from 'react';
import 'antd/dist/antd.css';
import { Upload, Icon } from 'antd';
const Upath = require('upath');
const { Dragger } = Upload;
class DropFile extends React.Component {
  beforeUpload = (file, fileList) => {
    if (file && file.path.length > 0) {
      const fileUploadPath = Upath.normalize(file.path)
      this.props.changeFile(fileUploadPath);
    } else {
      this.props.changeFile('');
    }
    return false
  }
  render() {
    const { name } = this.props;
    const { fileType } = this.props;
    const props = {
      multiple: false,
      id: name,
      accept: fileType,
      showUploadList: false,
      beforeUpload: this.beforeUpload
    };
    return (
      <Dragger {...props} >
        <p className="ant-upload-drag-icon">
          <Icon style={{ fontSize: 30}} type="inbox" />
        </p>
        <p style={{ color: 'rgba(0, 0, 0, 0.65)', fontSize: 13 }} className="ant-upload-text">クリックしてファイルを選択 もしくは ここにドラッグ＆ドロップ</p>
      </Dragger>
    )
  }
}
export default (DropFile);
