import React from 'react';
import { withTranslation } from 'react-i18next';
import 'antd/dist/antd.css';
import { Table, Input, Popconfirm, Form, } from 'antd';
import { COLUME_LARGE } from '../config/constants';
const { TextArea } = Input;

const EditableContext = React.createContext();
const EditableRow = ({ form, index, ...props }) => (
    <EditableContext.Provider value={form}>
        <tr {...props} />
    </EditableContext.Provider>
);
const EditableFormRow = Form.create()(EditableRow);

class EditableCell extends React.Component {
    state = {
        editing: false,
    };

    toggleEdit = () => {
        const editing = !this.state.editing;
        this.setState({ editing }, () => {
            if (editing) {
                this.input.focus();
            }
        });
    };

    save = e => {
        const { record, handleSave } = this.props;
        this.form.validateFields((error, values) => {
            if (error && error[e.currentTarget.id]) {
                return;
            }
            this.toggleEdit();
            handleSave({ ...record, ...values });
        });
    };

    renderCell = form => {
        this.form = form;
        const { children, dataIndex, record } = this.props;
        const { editing } = this.state;
        return editing ? (
            <Form.Item style={{ margin: 0 }} >
                {form.getFieldDecorator(dataIndex, {
                    initialValue: record[dataIndex],
                })(
                    <TextArea rows={15} ref={node => (this.input = node)} onPressEnter={this.save} onBlur={this.save} />
                )}
            </Form.Item>
        ) : (
                <div
                    className="editable-cell-value-wrap"
                    onClick={this.toggleEdit}
                >
                    {children}
                </div>
            );
    };

    render() {
        const {
            editable,
            dataIndex,
            title,
            record,
            index,
            handleSave,
            children,
            ...restProps
        } = this.props;
        return (
            <td {...restProps} style={{ padding: 0 }} className="cell-table">
                {editable ? (
                    <EditableContext.Consumer>{this.renderCell}</EditableContext.Consumer>
                ) : (
                        children
                    )}
            </td>
        );
    }
}
class TableComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            columns: [],
            dataTable: []
        }
    }

    componentWillReceiveProps(newProps) {
        if (typeof newProps.dataTable === 'undefined') return
        if (newProps.dataTable && newProps.dataTable.length > 0) {
            this.handleData(newProps.dataTable)
        }
        // else if (newProps.dataTable.length === 0 && this.props.data.length > 0) {
        //     this.handleData(this.props.data)
        // }
    }

    handleData(datas) {
        if (datas.length === 0) return
        if (datas.length > 0) {
            const options = {};
            const isHeader = (typeof options.headers === 'undefined' || options.headers) ? true : false;
            const [headers] = datas;
            delete headers.key
            delete headers.エラー内容
            var allColumns = Object.keys(headers)
            var columns = allColumns.map((index) => {
                if (COLUME_LARGE.includes(index) === true) {
                    return {
                        title: (isHeader) ? index : '',
                        dataIndex: index,
                        width: 550,
                        // editable: true,
                        align: 'center',
                    }
                } else {
                    return {
                        title: (isHeader) ? index : '',
                        dataIndex: index,
                        width: 180,
                        // editable: true,
                        align: 'center',
                    }
                }
            })
        }
        this.setState({
            columns: columns,
            data: Object.keys(datas).map((index) => {
                const item = datas[index];
                item.key = index;
                return item;
            }),
        });
    }

    handleDelete = key => {
        const data = [...this.state.data];
        const revertData = data.filter(item => item.key !== key)
        this.setState({
            data: revertData,
        });
        this.props.getTableData(revertData);
    };

    edit(key) {
        this.setState({ editingKey: key });
    }

    handleSave = row => {
        const newData = [...this.state.data];
        const index = newData.findIndex(item => row.key === item.key);
        if (index > -1) {
            const item = newData[index];
            newData.splice(index, 1, {
                ...item,
                ...row,
            });
        } else {
            newData.push(row);
        }
        this.props.getTableData(newData);
        this.setState({ data: newData });
    };
    render() {
        const components = {
            body: {
                row: EditableFormRow,
                cell: EditableCell,
            },
        };
        const columns = this.state.columns.map(col => {
            if (!col.editable) {
                return col;
            }
            return {
                ...col,
                onCell: record => ({
                    record,
                    editable: col.editable,
                    dataIndex: col.dataIndex,
                    title: col.title,
                    handleSave: this.handleSave,
                }),
            };
        });
        return (
            <div>
                <Table
                    components={components}
                    rowClassName={() => 'editable-row'}
                    scroll={{ x: 'calc(700px + 50%)', y: 500 }}
                    bordered
                    dataSource={this.state.data}
                    columns={columns}
                    pagination={false}
                />
            </div>
        );
    }
}

const TableComponentForm = Form.create()(TableComponent);
export default withTranslation('common')(TableComponentForm);
