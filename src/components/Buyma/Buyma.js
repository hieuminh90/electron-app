import React from 'react';
import 'antd/dist/antd.css';
import { Form, Input, Select, Button, Spin, Icon } from 'antd';
import './buyma.css';
import { withTranslation } from 'react-i18next';
import DataTable from '../../shared/SharedTable';
import {
  readDataFile,
  readFileCsv,
  downloadFileCSV,
  writeFileInput,
} from '../../util/file';
import {
  SCRIPT_NAME,
} from '../../config/constants';
import { spawnAlert } from '../../util/common';
import { runScript } from '../../util/electron';
const { ipcRenderer } = window.require('electron');


require('dotenv').config();
const { Option } = Select;
const FileSaver = require('file-saver');

class BuyMaForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      url: '',
      website: 'rakuten',
      urlError: false,
      tableData: [],
      isRunningScript: false,
      totalProductUploaded: null,
      productUploaded: null,
    };
  }

  componentDidMount() {
    // readDataFile(SCRIPT_NAME.SEARCH_SCRIPT.name, SCRIPT_NAME.SEARCH_SCRIPT.file.input_text).then(res => {
    //   this.setState({
    //     url: res[0],
    //   });
    // });
    const options = {};
    const replyChanel = "READ_FILE_CSV_REPLY";
    readFileCsv(SCRIPT_NAME.SEARCH_SCRIPT.name, options, SCRIPT_NAME.SEARCH_SCRIPT.file.result_csv, replyChanel).then((res) => {
      if (res.length > 0) {
        this.setState({
          tableData: res
        })
      } else {
        this.setState({
          tableData: []
        })
      }
    })
  }

  watchFileStatus = (script, file) => {
    return new Promise(() => {
      try {
        const replyChanel = 'WATCH_FILE_STATUS_REPLY';
        const args = {
          scriptName: script,
          file: file,
          reply: replyChanel,
        };
        ipcRenderer.send('WATCH_FILE', args);
        ipcRenderer.on(replyChanel, (event, res) => {
          if (res) {
            const result = res.toString().split('/');
            this.setState({
              totalProductUploaded: result[0],
              productUploaded: result[1],
            });
          }
        });
      } catch (error) {
        return error;
      }
    });
  };

  handeScrawlData = () => {
    const { url, website } = this.state
    if (url === '') {
      this.setState({
        urlError: true,
      })
      return
    } else {
      writeFileInput(this.state.url, SCRIPT_NAME.SEARCH_SCRIPT.name, SCRIPT_NAME.SEARCH_SCRIPT.file.input_text);
      this.setState({
        isRunningScript: true,
        totalProductUploaded: null,
        productUploaded: null,
        tableData: []
      })
      this.watchFileStatus(SCRIPT_NAME.SEARCH_SCRIPT.name, SCRIPT_NAME.SEARCH_SCRIPT.file.status)
      runScript(SCRIPT_NAME.SEARCH_SCRIPT.name, website).then(rs => {
        if (rs) {
          this.setState({
            isRunningScript: false
          })
          if (rs.code === 0) {
            const options = {};
            const replyChanel = "READ_FILE_CSV_REPLY";
            readFileCsv(SCRIPT_NAME.SEARCH_SCRIPT.name, options, SCRIPT_NAME.SEARCH_SCRIPT.file.result_csv, replyChanel).then((res) => {
              if (res.length > 0) {
                this.setState({
                  tableData: res
                })
                spawnAlert(
                  'success',
                  'Crawl data success',
                  'success',
                );
              } else {
                this.setState({
                  tableData: []
                })
                spawnAlert(
                  'error',
                  'Crawl data failed',
                  'error',
                );
              }
            })
          } else {
            spawnAlert(
              'error',
              'Crawl data failed',
              'error',
            );
          }
        }
      })
        ;
    }
  }

  downloadFileCsv() {
    downloadFileCSV(SCRIPT_NAME.SEARCH_SCRIPT.name, SCRIPT_NAME.SEARCH_SCRIPT.file.result_csv)
      .then(res => {
        const content = res.content.length > 0 ? res.content : '';
        var file = new File([content], res.fileName, { type: 'text/plain;charset=utf-8' });
        FileSaver.saveAs(file);
      })
      .catch(err => {
        spawnAlert('エラー', 'ファイルは存在しません', 'error');
      });
  }

  render() {
    const antIcon = <Icon type="loading" style={{ fontSize: 36, color: 'gray' }} spin />;
    const { urlError, url, isRunningScript } = this.state
    return (
      <div className="main">
        <div className="header">
          <div className="header__btn-csv" style={{ marginBottom: 10 }} >
            <Button type="primary" onClick={this.downloadFileCsv}>DOWNLOAD CSV</Button>
          </div>
          <div className="script">
            <div className="script__select" style={{ marginBottom: 10 }}>
              <Select defaultValue="rakuten" style={{ width: '100%' }} onSelect={
                (value) => {
                  this.setState({
                    website: value
                  })
                }
              }>
                <Option value="rakuten">Rakuten</Option>
                <Option value="amazon">Amazon</Option>
                <Option value="fril">Fril</Option>
              </Select>
            </div>
            <div className="script__input">
              <Input placeholder="テキストをご入力ください" value={url} onChange={(e) => {
                if (e.target.value) {
                  this.setState({
                    url: e.target.value,
                    urlError: false
                  })
                } else {
                  this.setState({
                    url: e.target.value,
                    urlError: true
                  })
                }
              }} />
              {urlError && (
                <div style={{ color: 'red' }}>Please enter Website URL</div>
              )}
            </div>
            <div className="script__button">
              <Button disabled={isRunningScript} type="primary" onClick={this.handeScrawlData}>GET DATA</Button>
            </div>
          </div>
        </div>
        <div className="data-table">
          <div className="data-table__header">
            <div>
              List Data
            </div>
            {this.state.totalProductUploaded !== null && (
              <div className="product-uploaded">
                <span>
                  {this.state.productUploaded}/{this.state.totalProductUploaded}
                </span>
              </div>
            )}
          </div>
          {
            isRunningScript ? (
              <div className="loading" >
                <Spin style={{ color: "gray" }} tip="Loading..." indicator={antIcon}>
                </Spin>
              </div>
            ) : (
                <div className="data-table__body">
                  <DataTable dataTable={this.state.tableData} />
                </div>
              )
          }
        </div>
      </div>
    );
  }
}

const BuyMa = Form.create({ name: 'register_buyma' })(BuyMaForm);
export default withTranslation('common')(BuyMa);
