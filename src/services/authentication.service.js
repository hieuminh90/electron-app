import { BehaviorSubject } from 'rxjs';
import { handleResponse } from '../helpers/handle-response';
import axios from '../util/axios';
const user = localStorage.getItem('currentUser');
const currentUserSubject = new BehaviorSubject((user) ? user : null);

export const authenticationService = {
    login,
    logout,
    checkMacAddress,
    updateMacAddress,
    currentUser: currentUserSubject.asObservable(),
    get currentUserValue() { return currentUserSubject.value }
};

function checkMacAddress(macAddress) {
    return axios.post('/api/mac_address/checking', { mac_address: macAddress }).then((response) => {
        const data = response.data;
        if (response.statusText !== 'OK') {
            if ([401, 403].indexOf(response.status) !== -1) {
                // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
                authenticationService.logout();
                // eslint-disable-next-line no-restricted-globals
                // location.reload(false);
                this.props.history.push('/login');
            }
            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }
        return data;
    }).then(rs => {
        axios.defaults.headers.common['Authorization'] = rs.token;
        localStorage.setItem('currentUser', JSON.stringify(rs.user));
        localStorage.setItem('token', rs.token);
        localStorage.setItem('token_mac', rs.token);
        currentUserSubject.next(rs.user);
        return rs;
    })
}


function updateMacAddress(macAddress, token) {
    return axios.post('/api/macaddress/update', { mac_address: macAddress }, {
        withCredentials: false,
        headers: {
            'Authorization': `Bearer ${token}`,
        }
    }).then(handleResponse)
        .then(rs => {
            return rs;
        })
}

function login(params) {
    return axios.post('/api/auth/login', params, {
        withCredentials: false
    }).then(handleResponse)
        .then(user => {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            axios.defaults.headers.common['Authorization'] = user.token;
            localStorage.setItem('currentUser', JSON.stringify(user));
            localStorage.setItem('token', user.token);
            currentUserSubject.next(user);
            return user;
        }).catch((err) => {
            return Promise.reject(err);
        })
}

function logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    localStorage.removeItem('token');
    currentUserSubject.next(null);
}