import axios from '../util/axios';

const ScriptService = {
  getLimitProductByScript(scriptId) {
    return axios.get(`/api/product/${scriptId}/remaining`, {}).then(response => {
      return response.data;
    });
  },
  updateProductLimit(scriptId, products) {
    return axios.post(`/api/product/update`, { script_id: scriptId, amount_of_run: products }).then(response => {
      return response.data;
    });
  },
  getConfigEachTool() {
    return axios.get(`/api/script/status`).then(response => {
      return response.data;
    });
  },
  getFileEachTool(scriptId) {
    return axios.get(`/api/script/${scriptId}/configurations`, { script_id: scriptId }).then(response => {
      return response.data;
    });
  },
  getFileData(fileUrl) {
    return axios.get(`${fileUrl}`).then(response => {
      return response.data;
    });
  },
  checkStockLicense(scriptId) {
    return axios.post(`/api/script/${scriptId}/stock`, { script_id: scriptId}).then(response => {
      return response.data;
    });
  },
  getDataFileLogTxt(scriptId){
    return axios.get(`/api/get/${scriptId}/logs`, { script_id: scriptId}).then(response => {
      return response.data;
    });
  },
  updateDataFileLogTxt(scriptId, data){
    const formData = new FormData()
    formData.append('file_log', data.file_log)
    return axios.post(`/api/sync/${scriptId}/logs`, formData).then(response => {
      return response.data;
    }).catch(err =>{
      console.log(err.response)
    });
  },
  getDataFileRuby(){
    return axios.get(`/api/scripts/rb`).then(response => {
      return response.data;
    });
  },
  getDataEachFileRuby(fileUrl){
    return axios.get(`${fileUrl}`).then(response => {
      return response.data;
    });
  },
  autocompleteTemplate(data){
    return axios.get(`/api/template`, { params: {type: data.type, description: data.description}}).then(response => {
      return response.data;
    });
  }
};
export default ScriptService;
