import React from 'react';
import './App.css';
import _ from 'lodash';
import { Layout } from 'antd';
import { withTranslation } from 'react-i18next';
import Main from '../src/pages/Main/Main'
require('dotenv').config()
const { Content } = Layout;
class App extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="buyma-main">
        <Layout>
          <Content style={{ height: 'auto' }}>
            <Main />
          </Content>
        </Layout>
      </div>
    );
  }
}
export default withTranslation('common')(App);
