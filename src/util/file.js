import moment from 'moment';
const Upath = require('upath');
const { ipcRenderer } = window.require('electron');

export async function writeFileInput(content, scriptName, fileName) {
  const args = {
    scriptName: scriptName,
    content: content,
    fileName: fileName,
  };
  ipcRenderer.send('write-file-input', args);
}

export async function updateFileImageSize(content, scriptName) {
  const args = {
    scriptName: scriptName,
    content: content,
  };
  ipcRenderer.send('write_image_setting', args);
}

export async function updateFileInput(filePath, scriptName, fileName) {
  const args = {
    scriptName: scriptName,
    filePath: filePath,
    fileName: fileName,
  };
  ipcRenderer.send('write-input', args);
}

export async function updateFileCsv(content, scriptName, path, file, isSettingFolder) {
  const convertContent = [];
  if (content.length > 0) {
    content.forEach(element => {
      delete element.key;
      delete element.エラー内容;
      convertContent.push(element);
    });
  } else {
    return;
  }
  const args = {
    scriptName: scriptName,
    content: convertContent,
    path: path,
    file: file,
    isSettingFolder: isSettingFolder,
  };
  ipcRenderer.send('update-file-upload', args);
}

export async function updateFileTemplateCsv(content, scriptName, file, replyChanel) {
  return new Promise((resolve, reject) => {
    try {
      const convertContent = [];
      if (content.length > 0) {
        content.forEach(element => {
          delete element.key;
          convertContent.push(element);
        });
      } else {
        return;
      }
      const args = {
        scriptName: scriptName,
        content: convertContent,
        file: file,
        reply: replyChanel,
      };
      ipcRenderer.send('UPDATE_FILE_TEMPLATE_CSV', args);
      ipcRenderer.once(replyChanel, (event, res) => {
        resolve(res);
      });
    } catch (error) {
      reject(error);
    }
  });
}

export async function updatePriceCsv(content, scriptName, file, replyChanel) {
  return new Promise((resolve, reject) => {
    try {
      const convertContent = [];
      if (content.length > 0) {
        content.forEach(element => {
          delete element.key;
          convertContent.push(element);
        });
      } else {
        return;
      }
      const args = {
        scriptName: scriptName,
        content: convertContent,
        file: file,
        reply: replyChanel,
      };
      ipcRenderer.send('UPDATE_FILE_PRICE', args);
      ipcRenderer.once(replyChanel, (event, res) => {
        resolve(res);
      });
    } catch (error) {
      reject(error);
    }
  });
}

export async function createHeaderFooterFileSize(headerText, footerText, scriptName, fileName) {
  const args = {
    header: headerText,
    footer: footerText,
    scriptName: scriptName,
    fileName: fileName,
  };
  ipcRenderer.send('create-header-footer-size', args);
}

export async function createHeaderFooterFileComment(headerText, footerText, scriptName, fileName) {
  const args = {
    header: headerText,
    footer: footerText,
    scriptName: scriptName,
    fileName: fileName,
  };
  ipcRenderer.send('create-header-footer-comment', args);
}

export async function copyFile(file, scriptName, fileName, optFolder = null) {
  const args = {
    file: file,
    scriptName: scriptName,
    fileName: fileName,
    optFolder: optFolder,
  };
  ipcRenderer.send('copy-file', args);
  return false;
}

export async function copyFileResult(baseFile, scriptName, newFile) {
  const args = {
    baseFile: baseFile,
    scriptName: scriptName,
    newFile: newFile,
  };
  ipcRenderer.send('copy-file-result', args);
  return false;
}

export async function copyFileAccount(file, scriptName, fileName) {
  const args = {
    file: file,
    scriptName: scriptName,
    fileName: fileName,
  };
  ipcRenderer.send('copy-file-account', args);
  return false;
}
export async function writeFile(content, scriptName, fileName) {
  const args = {
    scriptName: scriptName,
    content: content,
    fileName: fileName,
  };
  ipcRenderer.send('write-file', args);
}

export async function writeFileRuby(content, scriptName, fileName) {
  const args = {
    scriptName: scriptName,
    content: content,
    fileName: fileName,
  };
  ipcRenderer.send('write-file-ruby', args);
}
export async function writeFileSeaSon(content, scriptName, fileName) {
  const args = {
    scriptName: scriptName,
    content: content,
    fileName: fileName,
  };
  ipcRenderer.send('write-file-season', args);
}
export function readFileFarfetchAccount(scriptName, fileName) {
  return new Promise((resolve, reject) => {
    try {
      const replyChanel = 'read-file-farfetch-account-reply';
      const args = {
        scriptName: scriptName,
        fileName: fileName,
        optFolder: null,
        reply: replyChanel,
      };
      ipcRenderer.send('read-data-file', args);
      ipcRenderer.on(replyChanel, (event, res) => {
        resolve(res);
      });
    } catch (error) {
      reject(error);
    }
  });
}

export function readDataFile(scriptName, fileName) {
  return new Promise((resolve, reject) => {
    try {
      const replyChanel = 'read-data-file-reply';
      const args = {
        scriptName: scriptName,
        fileName: fileName,
        optFolder: null,
        reply: replyChanel,
      };
      ipcRenderer.send('read-data-file', args);
      ipcRenderer.on(replyChanel, (event, res) => {
        resolve(res);
      });
    } catch (error) {
      reject(error);
    }
  });
}

export function readDataCommentFile(scriptName, fileName) {
  return new Promise((resolve, reject) => {
    try {
      const args = {
        scriptName: scriptName,
        fileName: fileName,
        optFolder: null,
      };
      ipcRenderer.send('read-data-file-comment', args);
      ipcRenderer.on('read-data-file-comment-reply', (event, res) => {
        const data = {
          header: res.header,
          footer: res.footer,
        };
        resolve(data);
      });
    } catch (error) {
      reject(error);
    }
  });
}

export function readDataSizeFile(scriptName, fileName) {
  return new Promise((resolve, reject) => {
    try {
      const args = {
        scriptName: scriptName,
        fileName: fileName,
        optFolder: null,
      };
      ipcRenderer.send('read-data-file-size', args);
      ipcRenderer.on('read-data-file-size-reply', (event, res) => {
        const data = {
          header: res.header,
          footer: res.footer,
        };
        resolve(data);
      });
    } catch (error) {
      reject(error);
    }
  });
}
export async function readFilePrice(scriptName, options = {}, file, replyChanel) {
  return new Promise((resolve, reject) => {
    try {
      const args = {
        scriptName: scriptName,
        file: file,
        reply: replyChanel,
        options: options,
      };
      ipcRenderer.send('READ_FILE_PRICE', args);
      ipcRenderer.once(replyChanel, (event, res) => {
        const data = res;
        resolve(data);
      });
    } catch (error) {
      reject(error);
    }
  });
}

export async function readFileCsv(scriptName, options = {}, file, replyChanel) {
  return new Promise((resolve, reject) => {
    try {
      const args = {
        scriptName: scriptName,
        file: file,
        reply: replyChanel,
        options: options,
      };
      ipcRenderer.send('READ_FILE_TEMPLATE_CSV', args);
      ipcRenderer.once(replyChanel, (event, res) => {
        resolve(res);
      });
    } catch (error) {
      reject(error);
    }
  });
}

export function deleteFile(scriptName, fileDelete) {
  return new Promise((resolve, reject) => {
    try {
      const args = {
        scriptName: scriptName,
        files: fileDelete,
        optFolder: null,
      };
      ipcRenderer.send('DELETE_FILE', args);
    } catch (error) {
      reject(error);
    }
  });
}

export function downloadFile(scriptName, fileCopy) {
  return new Promise((resolve, reject) => {
    try {
      const replyChanel = 'downloadFileReply';
      const args = {
        scriptName: scriptName,
        fileCopy: fileCopy,
        optFolder: null,
        reply: replyChanel,
      };
      ipcRenderer.send('DOWNLOAD_FILE', args);
      ipcRenderer.once(replyChanel, (event, res) => {
        resolve(res);
      });
    } catch (error) {
      reject(error);
    }
  });
}

export function downloadFileCSV(scriptName, fileCopy) {
  return new Promise((resolve, reject) => {
    try {
      const replyChanel = 'DOWNLOAD_FILE_CSV_REPLY';
      const args = {
        scriptName: scriptName,
        fileCopy: fileCopy,
        optFolder: null,
        reply: replyChanel,
      };
      ipcRenderer.send('DOWNLOAD_FILE_CSV', args);
      ipcRenderer.once(replyChanel, (event, res) => {
        resolve(res);
      });
    } catch (error) {
      reject(error);
    }
  });
}

export function zipFolder(scriptName, folderZip) {
  return new Promise((resolve, reject) => {
    try {
      const replyChanel = 'ZIP_FOLDER_REPLY';
      const args = {
        scriptName: scriptName,
        folderZip: folderZip,
        reply: replyChanel,
      };
      ipcRenderer.send('ZIP_FOLDER', args);
      ipcRenderer.once(replyChanel, (event, res) => {
        resolve(res);
      });
    } catch (error) {
      reject(error);
    }
  });
}

export function zipFolderOutput(scriptName, folderZip, subFolder) {
  return new Promise((resolve, reject) => {
    try {
      const replyChanel = 'ZIP_FOLDER_OUTPUT_REPLY';
      const args = {
        scriptName: scriptName,
        folderZip: folderZip,
        subFolder: subFolder,
        reply: replyChanel,
      };
      ipcRenderer.send('ZIP_FOLDER_OUTPUT', args);
      ipcRenderer.once(replyChanel, (event, res) => {
        resolve(res);
      });
    } catch (error) {
      reject(error);
    }
  });
}

export function deleteFolder(scriptName, folderDelete) {
  return new Promise((resolve, reject) => {
    try {
      const args = {
        scriptName: scriptName,
        folderDelete: folderDelete,
        optFolder: null,
      };
      ipcRenderer.send('DELETE_FOLDER', args);
    } catch (error) {
      reject(error);
    }
  });
}

export function deleteFolderFrame(scriptName, folderDelete) {
  return new Promise((resolve, reject) => {
    try {
      const args = {
        scriptName: scriptName,
        folderDelete: folderDelete,
        optFolder: null,
      };
      ipcRenderer.send('DELETE_FOLDER_FRAME', args);
    } catch (error) {
      reject(error);
    }
  });
}

// export function addMoreFolder(scriptName, folderAdd) {
// 	return new Promise((resolve, reject) => {
// 		try {
// 			const args = {
// 				scriptName: scriptName,
// 				folderAdd: folderAdd,
// 			}
// 			ipcRenderer.send('ADD_MORE_FOLDER', args);
// 		} catch (error) {
// 			reject(error);
// 		}
// 	})
// }

export function checkEmptyFolder(scriptName, folderCheck) {
  return new Promise((resolve, reject) => {
    try {
      const replyChanel = 'CHECK_FOLDER_EMPTY_REPLY';
      const args = {
        scriptName: scriptName,
        folderCheck: folderCheck,
        reply: replyChanel,
      };
      ipcRenderer.send('CHECK_FOLDER_EMPTY', args);
      ipcRenderer.on(replyChanel, (event, res) => {
        resolve(res);
      });
    } catch (error) {
      reject(error);
    }
  });
}

export function checkEmptyFolderFrame(scriptName, folderCheck) {
  return new Promise((resolve, reject) => {
    try {
      const replyChanel = 'CHECK_FOLDER_FRAME_EMPTY_REPLY';
      const args = {
        scriptName: scriptName,
        folderCheck: folderCheck,
        reply: replyChanel,
      };
      ipcRenderer.send('CHECK_FOLDER_FRAME_EMPTY', args);
      ipcRenderer.on(replyChanel, (event, res) => {
        resolve(res);
      });
    } catch (error) {
      reject(error);
    }
  });
}

export function checkEmptyFolderProductImage(scriptName, folderCheck) {
  return new Promise((resolve, reject) => {
    try {
      const replyChanel = 'CHECK_FOLDER_PRODUCT_IMAGE_EMPTY_REPLY';
      const args = {
        scriptName: scriptName,
        folderCheck: folderCheck,
        reply: replyChanel,
      };
      ipcRenderer.send('CHECK_FOLDER_PRODUCT_IMAGE_EMPTY', args);
      ipcRenderer.on(replyChanel, (event, res) => {
        resolve(res);
      });
    } catch (error) {
      reject(error);
    }
  });
}

export async function searchLogTxt(scriptName, file, startDate, endDate) {
  return new Promise((resolve, reject) => {
    try {
      const replyChanel = 'SEARCH_LOG_TXT_REPLY';
      const args = {
        scriptName: scriptName,
        file: file,
        reply: replyChanel,
        optFolder: null,
      };
      ipcRenderer.send('SEARCH_LOG_TXT', args);
      ipcRenderer.on(replyChanel, (event, res) => {
        var dataSearch = [];
        var allData = res.map(item => {
          const indexOffFirstEnter = item.toString().indexOf('\n');
          return {
            dateTime: item.substr(0, indexOffFirstEnter),
            url: item.substr(indexOffFirstEnter + 1, item.length - 1).replace(/,/g, '\n'),
          };
        });
        if (endDate === null || endDate === '') {
          const startDateMoment = moment(startDate);
          allData.forEach(item => {
            const dateTime = moment(item.dateTime).startOf('day');
            if (dateTime.diff(startDateMoment, 'days') >= 0) {
              dataSearch.push(item);
            }
          });
        } else {
          const startDateMoment = moment(startDate);
          const endDateMoment = moment(endDate);
          allData.forEach(item => {
            const dateTime = moment(item.dateTime).startOf('day');
            if (
              dateTime.diff(startDateMoment, 'days') >= 0 &&
              endDateMoment.diff(dateTime, 'days') >= 0
            ) {
              dataSearch.push(item);
            }
          });
        }
        resolve(dataSearch);
      });
    } catch (error) {
      reject(error);
    }
  });
}
export async function deleteRecordLogTxt(scriptName, file, content) {
  return new Promise((resolve, reject) => {
    try {
      const replyChanel = 'DELETE_RECORD_IN_LOG_TXT_REPLY';
      const contentToString = content.dateTime + '\n' + content.url;
      const args = {
        scriptName: scriptName,
        content: contentToString,
        file: file,
        optFolder: null,
        reply: replyChanel,
      };
      ipcRenderer.send('DELETE_RECORD_IN_LOG_TXT', args);
      ipcRenderer.once(replyChanel, (event, fileContent) => {
        var file = new File([fileContent], 'log.txt', {
          type: 'text/plain',
        });
        resolve(file);
      });
    } catch (error) {
      reject(error);
    }
  });
}

export async function deleteOneLineLogTxt(scriptName, file, oldRecord, newRecord) {
  if (newRecord.url.trim()) {
    return new Promise((resolve, reject) => {
      try {
        const replyChanel = 'DELETE_ONE_RECORD_IN_LOG_TXT_REPLY';
        const regex = /\n{2,}/g;
        const oldRecordToString = oldRecord.dateTime + '\n' + oldRecord.url;
        const newRecordToString =
          newRecord.dateTime + '\n' + newRecord.url.trim().replace(regex, '\n');
        const args = {
          scriptName: scriptName,
          oldRecord: oldRecordToString,
          newRecord: newRecordToString.trim(),
          file: file,
          optFolder: null,
          reply: replyChanel,
        };
        ipcRenderer.send('DELETE_ONE_RECORD_IN_LOG_TXT', args);
        ipcRenderer.once(replyChanel, (event, fileContent) => {
          var file = new File([fileContent], 'log.txt', {
            type: 'text/plain',
          });
          resolve(file);
        });
      } catch (error) {
        reject(error);
      }
    });
  } else {
    return new Promise((resolve, reject) => {
      try {
        const replyChanel = 'DELETE_ONE_RECORD_IN_LOG_TXT_REPLY';
        const oldRecordToString = oldRecord.dateTime + '\n' + oldRecord.url;
        const args = {
          scriptName: scriptName,
          content: oldRecordToString,
          file: file,
          optFolder: null,
          reply: replyChanel,
        };
        ipcRenderer.send('DELETE_RECORD_IN_LOG_TXT', args);
        ipcRenderer.once(replyChanel, (event, fileContent) => {
          var file = new File([fileContent], 'log.txt', {
            type: 'text/plain',
          });
          resolve(file);
        });
      } catch (error) {
        reject(error);
      }
    });
  }
}

export async function readFilePriceItemCheck(scriptName, subFolder, options = {}, file) {
  return new Promise((resolve, reject) => {
    try {
      const replyChanel = 'READ_FILE_ITEM_CHECK_PRICE_CSV_REPLY';
      const args = {
        scriptName: scriptName,
        subFolder: subFolder,
        file: file,
        reply: replyChanel,
        options: options,
      };
      ipcRenderer.send('READ_FILE_ITEM_CHECK_PRICE_CSV', args);
      ipcRenderer.once(replyChanel, (event, res) => {
        const data = res;
        resolve(data);
      });
    } catch (error) {
      reject(error);
    }
  });
}

export async function updateFilePriceItemCheckCsv(
  content,
  scriptName,
  subFolder,
  file,
  replyChanel,
) {
  return new Promise((resolve, reject) => {
    try {
      const convertContent = [];
      if (content.length > 0) {
        content.forEach(element => {
          delete element.key;
          convertContent.push(element);
        });
      } else {
        return;
      }
      const args = {
        scriptName: scriptName,
        content: convertContent,
        file: file,
        subFolder: subFolder,
        reply: replyChanel,
      };
      ipcRenderer.send('UPDATE_FILE_ITEM_CHECK_PRICE_CSV', args);
      ipcRenderer.once(replyChanel, (event, res) => {
        resolve(res);
      });
    } catch (error) {
      reject(error);
    }
  });
}

export async function checkFileInput(scriptName, fileInput) {
  return new Promise((resolve, reject) => {
    try {
      const replyChanel = 'CHECK_FILE_INPUT_CSV_REPLY';
      const args = {
        scriptName: scriptName,
        file: fileInput,
        reply: replyChanel,
      };
      ipcRenderer.send('CHECK_FILE_INPUT_CSV', args);
      ipcRenderer.once(replyChanel, (event, res) => {
        const data = res;
        resolve(data);
      });
    } catch (error) {
      reject(error);
    }
  });
}

export function killProcess(scriptName, fileBin) {
  return new Promise((resolve, reject) => {
    try {
      const replyChanel = 'KILL_PROCESS_REPLY';
      const args = {
        fileBin: fileBin,
        scriptName: scriptName,
        reply: replyChanel,
      };
      ipcRenderer.send('KILL_PROCESS', args);
      ipcRenderer.once(replyChanel, (event, data) => {
        resolve(data);
      });
    } catch (error) {
      reject(error);
    }
  });
}

export function copyImageFooter(listFiles, destinationFolder) {
  const listFilePath = listFiles.map((file, index) => {
    return Upath.normalize(file.path)
  })
  const args = {
    listFiles: listFilePath,
    destinationFolder: destinationFolder,
  };
  ipcRenderer.send('COPY_IMAGE_FOOTER', args);
}
