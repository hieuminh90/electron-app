import axios from 'axios';
let headers = {
  'Content-Type': 'application/json',
};
if (localStorage.getItem('token')) {
  headers = {
    ...headers, ...{
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
    }
  }
}
let axiosOption = {
  withCredentials: true,
  headers: headers,
}
if (process.env.NODE_ENV === 'production') {
  axiosOption = { ...axiosOption, ...{ baseURL: 'https://localhost:300' } };
}
export default axios.create(axiosOption);
