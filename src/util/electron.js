const { ipcRenderer } = window.require("electron");
const crypto = require('crypto');
export async function getFirstImage(folder) {
	const args = {
		folder: folder
	}
	await ipcRenderer.send('GET_FIRST_IMAGE_OUT_FRAME', args);
	return new Promise((resolve, reject) => {
		try {
			ipcRenderer.once('GET_FIRST_IMAGE_OUT_FRAME_RESULT', (event, data) => {
				resolve(data);
			});
		} catch (error) {
			reject(error);
		}
	});
}
export async function runScript(scriptName, fileRuby) {
	return new Promise((resolve, reject) => {
		try {
			const replyChanel = `RUN_SCRIPT_${scriptName}_${crypto.randomBytes(64).toString('hex')}`;
			const args = {
				scriptName: scriptName,
				fileRuby: fileRuby,
				reply: replyChanel,
			}
			ipcRenderer.send('run-script', args);
			ipcRenderer.once(replyChanel, (event, data) => {
				resolve(data);
			})
		} catch (error) {
			reject(error)
		}
	});
}
export async function getMacAddress() {
	return new Promise((resolve, reject) => {
		try {
			ipcRenderer.send('GET_MAC_ADDRESS');
			ipcRenderer.once('GET_MAC_ADDRESS_REPLY', (event, data) => {
				resolve(data);
			});
		} catch (error) {
			reject(error);
		}
	});
}
export async function copyDir(baseDir, scriptName, newDir) {
	return new Promise((resolve, reject) => {
		try {
			const replyChanel = `COPY_DIR_${crypto.randomBytes(64).toString('hex')}`
			const args = {
				baseDir: baseDir,
				newDir: newDir,
				scriptName: scriptName,
				replyChanel: replyChanel
			}
			ipcRenderer.send('COPY_DIR', args);
			ipcRenderer.once(replyChanel, (data) => {
				resolve(data);
			})
		} catch (error) {
			reject(error)
		}
	})
}

export async function copyDirOutput(baseDir, scriptName, newDir) {
	return new Promise((resolve, reject) => {
		try {
			const args = {
				baseDir: baseDir,
				newDir: newDir,
				scriptName: scriptName,
			}
			ipcRenderer.send('COPY_DIR_OUT_PUT', args);
		} catch (error) {
			reject(error)
		}
	})
}

export async function updateFileLogTxt(scriptName, file) {
	const replyChanel = 'REPLY_UPDATE_DATA_FILE_LOG_TXT';
	return new Promise((resolve, reject) => {
		try {
			const args = {
				scriptName: scriptName,
				file: file,
				reply: replyChanel,
			}
			ipcRenderer.send('UPDATE_DATA_FILE_LOG_TXT', args);
			ipcRenderer.once(replyChanel, (event, newFile) => {
				var file = new File([newFile], "log.txt", {
					type: "text/plain",
				});
				resolve(file);
			})
		} catch (error) {
			reject(error)
		}
	})
}

export async function setttingPermissionGeckoDriver(GeckoDriverFiles) {
	return new Promise((resolve, reject) => {
		try {
			ipcRenderer.send('SETTING_PERMISSION_GECKO_DRIVER', GeckoDriverFiles);
		} catch (error) {
			reject(error)
		}
	})
}