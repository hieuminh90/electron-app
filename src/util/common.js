import { notification } from 'antd';
import ScriptService from '../services/script.service';

export function spawnAlert(title, content, type) {
  const args = {
    message: title,
    description: content,
    duration: 3
  };
  if (type === 'success') {
    return notification.success(args);
  } else if (type === 'error') {
    return notification.error(args);
  } else {
    return notification.open(args);
  }
}

export function getLimitProduct(scriptID) {
  return new Promise((resolve, reject) => {
    ScriptService.getLimitProductByScript(scriptID).then((productLimit) => {
      resolve(productLimit);
    }).catch(error => {
      reject(error)
    })
  })
}