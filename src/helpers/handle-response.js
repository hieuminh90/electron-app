import { authenticationService } from "../services";

export function handleResponse(response) {
    const data = response.data ;
    if (response.statusText !== 'OK') {
        if ([401, 403].indexOf(response.status) !== -1) {
            // auto logout if 401 Unauthorized or 403 Forbidden response returned from api
            authenticationService.logout();
            // eslint-disable-next-line no-restricted-globals
            location.reload(true);
            this.props.history.push('/login');
        }
        const error = (data && data.message) || response.statusText;
        return Promise.reject(error);
    }
    return data;
}