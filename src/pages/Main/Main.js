import React from 'react'
import { Tabs } from 'antd';
import 'antd/dist/antd.css';
import '../../components/Buyma/buyma.css';
import BuyMa from '../../components/Buyma/Buyma';
import { withTranslation } from 'react-i18next';
import { SCRIPT_TAB_KEY } from '../../config/constants';
const { TabPane } = Tabs;
class Main extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			activeTab: SCRIPT_TAB_KEY.BUY_MA,
		}
	}


	render() {
		return (
			<Tabs onTabClick={this.handleOnTabClick} activeKey={this.state.activeTab}>
				<TabPane tab='CRAWL APP' key={SCRIPT_TAB_KEY.BUY_MA}>
					<BuyMa />
				</TabPane>
			</Tabs >
		);
	}
}
export default withTranslation('common')(Main);